FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /app

COPY *.sln .
COPY OnlineLearningSolution/*.csproj ./OnlineLearningSolution/
RUN dotnet restore

COPY OnlineLearningSolution/. ./OnlineLearningSolution/
WORKDIR /app/OnlineLearningSolution
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS runtime
WORKDIR /app
COPY --from=build /app/OnlineLearningSolution/out ./
ENTRYPOINT ["dotnet", "OnlineLearningSolution.dll"]