#!/bin/bash

echo 'Please enter the image tag ?'
read image_tag

sudo docker build -t ibee:$image_tag .

sudo docker-compose down

echo IMAGE_NAME=ibee:$image_tag >> .env

sudo docker-compose up -d