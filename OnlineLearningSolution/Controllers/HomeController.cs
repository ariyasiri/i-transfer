﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineLearningSolution.Models;

namespace OnlineLearningSolution.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly LMSHttpServiceClinet _httpClinet;

        public HomeController(LMSHttpServiceClinet client)
        {
            _httpClinet = client;            
        }

        public async Task<ActionResult> Index()
        {
            return View();
        }
   

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
