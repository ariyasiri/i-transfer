﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OnlineLearningSolution.Controllers.Principal
{
    public class PrincipalController : Controller
    {
        // GET: Principal
        public ActionResult Index()
        {
            return View();
        }

        // GET: Principal/UnitLevel
        public ActionResult UnitLevel()
        {
            return View();
        }

        // GET: Principal/AverageResults
        public ActionResult AverageResults()
        {
            return View();
        }

        // GET: Principal/SubjectCoverage
        public ActionResult SubjectCoverage()
        {
            return View();
        }
    }
}