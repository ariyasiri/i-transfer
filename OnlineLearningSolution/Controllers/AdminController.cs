﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OnlineLearningSolution.Models;
using OnlineLearningSolution.Utilities;

namespace OnlineLearningSolution.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    public class AdminController : Controller
    {
        private readonly LMSHttpServiceClinet _client;

        public  AdminController(LMSHttpServiceClinet clinet)
        {
            _client = clinet;
        }

        // GET: Admin/CreateQuestionPaper
        [HttpGet]
        public ActionResult CreateQuestionPaper()
        {
            return View();
        }

        // GET: Admin/GetAllQualifications
        [HttpGet]
        public JsonResult GetAllQualifications()
        {
            string token = HttpContext.Session.GetString("AccessToken");
            var userLogin = TempData.Get<UserLogin>("UserData");
            var response = _client.GetAsync<List<Qulification>>(string.Format("/quizapp/api/{0}/qualifications", "v1.0"), token);
            return Json(response);
        }

        /// <summary>
        ///  GET: Admin/GetAllQualificationStageTerms
        /// </summary>
        /// <param name="qulifiacationId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetAllQualificationStageTerms(int qualificationId)
        {
            string token = HttpContext.Session.GetString("AccessToken");
            var response = _client.GetAsync<List<Qulification>>(string.Format("/quizapp/api/{0}/qualification_stage_terms?qualificationStageId={1}", "v1.0", qualificationId), token);
            return Json(response);
        }

        /// <summary>
        /// GET: Admin/GetAllQualificationStages
        /// </summary>
        /// <param name="qualificationId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetAllQualificationStages(int qualificationId)
        {
            string token = HttpContext.Session.GetString("AccessToken");
            var response = _client.GetAsync<List<Qulification>>(string.Format("/quizapp/api/{0}/qualification_stages?qualificationId=1{1}", "v1.0", qualificationId), token);
            return Json(response);
        }

        /// <summary>
        /// GET: Admin/GetAllQualificationSubjects
        /// </summary>
        /// <param name="qualificationId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetAllQualificationSubjects(int qualificationId)
        {
            string token = HttpContext.Session.GetString("AccessToken");
            var response = _client.GetAsync<List<Qulification>>(string.Format("/quizapp/api/{0}/qualification_subjects?qualificationId=", "v1.0", qualificationId), token);
            return Json(response);
        }


        // POST: /Admin/SavePaper
        [HttpPost]
        public async Task<JsonResult> SavePaper(Paper paper)
        {

            HttpContent content = new FormUrlEncodedContent(new[] {
                 new KeyValuePair<string, string>("Name",  paper.Name),
                  new KeyValuePair<string, string>("AllocatedTimeInMinutes",  paper.AllocatedTimeInMinutes.ToString()),
                   new KeyValuePair<string, string>("Description",  paper.Description),
                   new KeyValuePair<string, string>("NoOfQuestions",  paper.NoOfQuestions.ToString()),
                  new KeyValuePair<string, string>("QualificationRevisionId",  paper.QualificationRevisionId.ToString()),
                   new KeyValuePair<string, string>("QualificationStageTermId",  paper.QualificationStageTermId.ToString()),
                   new KeyValuePair<string, string>("QualificationSubjectId",  paper.QualificationSubjectId.ToString()),
                  new KeyValuePair<string, string>("UploadingPaper",  paper.UploadingPaper.ToString()),
                   new KeyValuePair<string, string>("consumerKey",  paper.Year.ToString())
            });

  
            var httpResponseMessage = await _client.Client.PostAsync(string.Format("/quizapp/api/{0}/papers", "v1.0"), content);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var userLogin = await httpResponseMessage.Content.ReadAsAsync<Paper>();
                return Json(userLogin);

            }
            return Json("Error");
        }

        // POST: /Admin/SavePaper
        [HttpPost]
        public async Task<JsonResult> SaveQuestion(PaperQuestion paperQuestion)
        {
            var client = new HttpClient();
            var values = new List<KeyValuePair<string, string>>();
          //  values.Add(new KeyValuePair<string, int>("PaperId", paperQuestion.PaperId));
          //  values.Add(new KeyValuePair<string, string>("Password", model.Password));
            var content = new FormUrlEncodedContent(values);

            var httpResponseMessage = await _client.Client.PostAsync(string.Format("/quizapp/api/{0}/paper_questions", "v1.0"), content);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var userLogin = await httpResponseMessage.Content.ReadAsAsync<Paper>();
                return Json(userLogin);

            }
            return Json("Error");
        }

        // POST: /Admin/SavePaper
        [HttpPost]
        public   IActionResult SaveQuestionAnswers(QuestionAnswer answer)
        {
            HttpContent content = new StringContent(JsonConvert.SerializeObject(answer), Encoding.UTF8, "application/json");

            var httpResponseMessage =   _client.PostAsync(string.Format("/quizapp/api/{0}/question_answers", "v1.0"), answer, "");

            return Json(httpResponseMessage);
        }

        private string GetToken(int tokenType)
        {
            switch (tokenType)
            {
                case 1: return HttpContext.Session.GetString("AccessToken");  
                  default:    return HttpContext.Session.GetString("AccessToken");   
            }
        }

    }
}