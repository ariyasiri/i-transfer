﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OnlineLearningSolution.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    public class QuizController : Controller
    {
        // GET: Quiz
        public ActionResult Index()
        {
            return View();
        }


        // GET: Quiz/CreateQuiz
        [HttpGet]
        public ActionResult CreateQuiz()
        {
            return View();
        }


         
    }
}