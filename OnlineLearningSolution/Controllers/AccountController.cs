﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OnlineLearningSolution.Models;
using OnlineLearningSolution.Utilities;
using static OnlineLearningSolution.Models.LoginViewModel;

namespace OnlineLearningSolution.Controllers
{
    public class AccountController : Controller
    { 
        private ClaimsIdentity identity = null;
        private bool isAuthenticated = false;
        private LMSHttpServiceClinet _client;
        private readonly IOptions<ElearningService> _appSettings;
 
        public AccountController(IOptions<ElearningService> appSettings, LMSHttpServiceClinet clinet)
        {
            _appSettings = appSettings;
            _client = clinet;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
       {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel());
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if(model.SelectedInstituteID >0)
            {
                LoginViewModel tempData = TempData.Get<LoginViewModel>("usermodel");
                tempData.SelectedInstituteID = model.SelectedInstituteID;
                model = tempData;
            }
 
            try
            {
 
                var values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("UserName", model.UserName));
                values.Add(new KeyValuePair<string, string>("Password", model.Password));
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
                 
                
                    UserLogin userLogin = new UserLogin() { Institutes = new List<Institute>(), SelectedInstituteID=1 };

                    if (userLogin.Institutes.Count == 1 || userLogin.SelectedInstituteID >0)
                    {
                        identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, model.UserName),
                    new Claim(ClaimTypes.Role, "Admin"), 
                    }, CookieAuthenticationDefaults.AuthenticationScheme);


                        HttpContext.Session.SetString("AccessToken", "AccessToken");
                        HttpContext.Session.SetString("RefreshToken", "RefreshToken");
                        HttpContext.Session.SetInt32("SelectedInstituteID",1);
                        TempData.Put<UserLogin>("UserData", userLogin);

                        isAuthenticated = true;
                    }
                    else if (userLogin.Institutes.Count > 0)
                    {
                        TempData.Put<LoginViewModel>("usermodel", model);                         
                        model.Institutes = userLogin.Institutes;
                        return View(model);
                    }

                
            }
            catch (Exception ex)
            {

            }
            
            if (isAuthenticated)
            {
                var principal = new ClaimsPrincipal(identity);

                var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                return RedirectToLocal(returnUrl);
            }

            ModelState.AddModelError("", "Invalid login attempt.");
            return View(model);

        }


        private async Task<bool> UserLogin(LoginViewModel model)
        {
            string usetrJson = JsonConvert.SerializeObject(model);
            HttpContent httpContent = new StringContent(usetrJson, Encoding.UTF8, "application/json");

            try
            {
                var httpResponseMessage = await _client.Client.PostAsync(string.Format("{0}/api/v{1}/user_logins/sign_in", _appSettings.Value.PathBase,_appSettings.Value.ApiVersion), httpContent);

                if (httpResponseMessage.IsSuccessStatusCode)
                 {
                    var logedinViewModel = await httpResponseMessage.Content.ReadAsAsync<UserLogin>();

                    identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, model.UserName),
                    new Claim(ClaimTypes.Role, "Admin"),
                    new Claim("AccessToken", logedinViewModel.AccessToken),
                    new Claim("FamilyName",logedinViewModel.FamilyName),
                    new Claim("FirstName", logedinViewModel.FirstName),
                    new Claim(ClaimTypes.DateOfBirth, Convert.ToDateTime(logedinViewModel.Birthday).ToString("yyyy/MM/dd")),
                    new Claim("OtherNames", logedinViewModel.OtherNames),
                    new Claim (ClaimTypes.Gender , logedinViewModel.Gender),
                    new Claim ("RefreshToken" , logedinViewModel.RefreshToken)
                }, CookieAuthenticationDefaults.AuthenticationScheme);

                    return true;
                }
            }
            catch(Exception ex)
            {

            }
            return false;
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// User logout
        /// </summary>
        /// <returns></returns>
        public IActionResult Logout()
        {
            var identity = (ClaimsIdentity)User.Identity;

            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }

        //GET: UserRegistration
        public ActionResult Register()
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (ModelState.IsValid)
            {      
                string usetrJson = JsonConvert.SerializeObject(model);
                HttpContent httpContent = new StringContent(usetrJson, Encoding.UTF8, "application/json");

                var httpResponseMessage = await _client.Client.PostAsync(string.Format("{0}/api/v{1}/students_registration", _appSettings.Value.PathBase,_appSettings.Value.ApiVersion), httpContent);

                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    var loginModel = new LoginViewModel()
                    {
                        UserName = model.Username,
                        Password = model.Password
                    };

                    await UserLogin(loginModel);

                    if (isAuthenticated)
                    {
                        var principal = new ClaimsPrincipal(identity);

                        var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Account", "Login");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ResetPassword
        [HttpGet]
        [Authorize]
        public   ActionResult ResetPassword()
        {
            return View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
          //  var user = await FindByNameAsync(model.Email);   need to implemnt
           //if successed show confirmation
            return View();
        }

    }
}