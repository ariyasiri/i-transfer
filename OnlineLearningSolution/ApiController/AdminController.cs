﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using OnlineLearningSolution.Models;


namespace OnlineLearningSolution.ApiController
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ApiContollerBase
    {
        private readonly LMSHttpServiceClinet _client;
        private readonly IOptions<ElearningService> _appSettings;

        public AdminController(IOptions<ElearningService> appSettings, LMSHttpServiceClinet clinet)
        {
            _client = clinet;
            _appSettings = appSettings;
        }

        [Route("GetPapersByParam")]
        [HttpGet]
        public async Task<PaperSearchResult> GetPapersByParam(string paramArgs)
        {
            string token = GetAccesssToken();
            var serachList = await _client.GetAsync<PaperSearchResult>(string.Format("{0}/api/{1}/papers/search?" + paramArgs, _appSettings.Value.PathBase, _appSettings.Value.ApiVersion), token);

            return serachList;
        }

        [Route("GetAllQualifications")]
        [HttpGet]
        public async Task<List<Qulification>> GetAllQualifications()
        {
            string token = GetAccesssToken();
            var response = await _client.GetAsync<List<Qulification>>(string.Format("{0}/api/{1}/qualifications", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion), token);

            return response.ToList();
        }

        [Route("GetQuestionAnswer")]
        [HttpGet]
        public async Task<List<QuestionAnswer>> GetQuestionAnswer(int questionId)
        {
            string token = GetAccesssToken();
            var response = await _client.GetAsync<List<QuestionAnswer>>(string.Format("{0}/api/{1}/question_answers?questionId={2}", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion, questionId), token);

            return response.ToList();
        }

        [Route("GetPaperQuestionById")]
        [HttpGet]
        public async Task<List<PaperQuestionModel>> GetPaperQuestionById(int paperId)
        {
            string token = GetAccesssToken();
            var response = await _client.GetAsync<List<PaperQuestionModel>>(string.Format("{0}/api/{1}/paper_questions?paperId={2}", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion, paperId), token);

            return response.ToList();
        }

        [Route("GetQualificationStageTerms")]
        [HttpGet]
        public List<QulificationTerm> GetQualificationStageTerms(int qualificationId)
        {
            string token = GetAccesssToken();
            var response = _client.GetAsync<List<QulificationTerm>>(string.Format("{0}/api/{1}/qualification_stage_terms?qualificationStageId={2}", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion, qualificationId), token);
            return response.Result.ToList();
        }

        [Route("GetUnitsBySubject")]
        [HttpGet]
        public List<Unit> GetUnitsBySubject(int subjectId)
        {

            List<Unit> unitList = new List<Unit>();
            string token = GetAccesssToken();
            var response = _client.GetAsync<List<SubjectUnit>>(string.Format("{0}/api/{1}/subject_unit?subjectId={2}", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion, subjectId), token);
            var list = response.Result.ToList();
            foreach (var item in list)
            {
                unitList.Add(item.Unit);
            }
            return unitList;
        }

        [Route("GetQualificationStages")]
        [HttpGet]
        public List<QulificationStage> GetQualificationStages(int qualificationId)
        {
            string token = GetAccesssToken();
            var response = _client.GetAsync<List<QulificationStage>>(string.Format("{0}/api/{1}/qualification_stages?qualificationId={2}", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion, qualificationId), token);
            return response.Result.ToList();
        }

        [Route("GetQualificationSubjects")]
        [HttpGet]
        public List<QulificationSubject> GetQualificationSubjects(int qualificationId)
        {
            string token = GetAccesssToken();
            var response = _client.GetAsync<List<QulificationSubject>>(string.Format("{0}/api/{1}/qualification_subjects?qualificationId={2}", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion, qualificationId), token);
            return response.Result.ToList();    
        }

        [Route("GetQualificationRevisions")]
        [HttpGet]
        public List<QulificationRevision> GetQualificationRevisions(int qualificationId)
        {
            string token = GetAccesssToken();
            var response = _client.GetAsync<List<QulificationRevision>>(string.Format("{0}/api/{1}/qualification_revisions?qualificationId={2}", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion, qualificationId), token);
            return response.Result.ToList();
        }

        // POST: api/SaveQuestionData
        [Route("SaveQuestionData")]
        [HttpPost]
        public async Task<PaperQuestion> SaveQuestionData()
        {

            try
            {
                PaperQuestion paperQuestion = new PaperQuestion();
                paperQuestion.PaperId = Convert.ToInt32(HttpContext.Request.Form["PaperId"]);
                paperQuestion.QuestionOrder = Convert.ToInt32(HttpContext.Request.Form["QuestionOrder"]);
                Question question = new Question();
                question.DifficultyLevel = HttpContext.Request.Form["DifficultyLevel"];
                question.Origin = HttpContext.Request.Form["Origin"];
                question.AllocatedTime = Convert.ToInt32(HttpContext.Request.Form["AllocatedTime"]);
                question.NoOfAnswers = Convert.ToInt32(HttpContext.Request.Form["NoOfAnswers"]);
                question.CorrectAnswer = Convert.ToInt32(HttpContext.Request.Form["CorrectAnswer"]);
                question.NoOfCorrectAnswers = 1;
                question.Description = HttpContext.Request.Form["Description"];
                question.Units = HttpContext.Request.Form["Units"];
                question.QuestionImg = Encoding.ASCII.GetBytes(HttpContext.Request.Form["QuestionImg"]);

                var baseImg = HttpContext.Request.Form["QuestionImg"].ToString().Replace("data:image/octet-stream;base64,", ""); 
                question.QuestionImg = Convert.FromBase64String(baseImg);

                using (var content = new MultipartFormDataContent())
                {
                    var values = new[]
                    {
                           new KeyValuePair<string, string>("PaperId",  paperQuestion.PaperId.ToString()),
                  new KeyValuePair<string, string>("QuestionOrder", paperQuestion.QuestionOrder.ToString()),
                   new KeyValuePair<string, string>("Description", question.Description),
                   new KeyValuePair<string, string>("Question.QuestionText",  question.QuestionText),
                  new KeyValuePair<string, string>("Question.DifficultyLevel",  question.DifficultyLevel),
                   new KeyValuePair<string, string>("Question.Origin",  question.Origin.ToString()),
                   new KeyValuePair<string, string>("Question.AllocatedTime",  question.AllocatedTime.ToString()),
                   new KeyValuePair<string, string>("Question.NoOfAnswers", question.NoOfAnswers.ToString()),
                    new KeyValuePair<string, string>("Question.NoOfCorrectAnswers",  question.NoOfCorrectAnswers.ToString()),
                     new KeyValuePair<string, string>("Question.Description",  question.Description.ToString()),
                     new KeyValuePair<string, string>("Question.UnitIds",  question.Units),
                       };

                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent((keyValuePair.Value) == null ? "" : keyValuePair.Value), keyValuePair.Key);
                    }

                    var fileContent = new ByteArrayContent(question.QuestionImg);

                    content.Add(fileContent, "Question.QuestionImg", "imageContent.png");

                    _client.Client.DefaultRequestHeaders.Accept.Clear();
                    _client.Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string token = GetAccesssToken();
                    if (!string.IsNullOrEmpty(token))
                    {
                        _client.Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                        var httpResponseMessage = await _client.Client.PostAsync(string.Format("{0}/api/{1}/paper_questions", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion), content);

                        if (httpResponseMessage.IsSuccessStatusCode)
                        {
                            var newQuestion = await httpResponseMessage.Content.ReadAsAsync<PaperQuestion>();
                            newQuestion.Question.CorrectAnswer = question.CorrectAnswer;
                            await SaveQuestionAnswer(newQuestion.Question);
                            return newQuestion;
                        }
                    }

                }
                return null;
            }
            catch (Exception ex) { }
            return null;

        }

        
        private async Task<List<QuestionAnswer>> SaveQuestionAnswer(Question question)
        {
            List<dynamic> qustionAnswers = new List<dynamic>();
            for (int i = 1; i <= question.NoOfAnswers; i++)
            {
                qustionAnswers.Add(new
                {
                    QuestionId = question.QuestionId,
                    AnswerOrder = i,
                    IsCorrectAnswer = (i == question.CorrectAnswer) ? true : false
                });
            }

            try
            {
                string token = GetAccesssToken();
                if (!string.IsNullOrEmpty(token))
                {

                    var httpResponseMessage = await _client.PostAsync(string.Format("{0}/api/{1}/question_answers", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion), qustionAnswers, token);

                    if (httpResponseMessage.Count > 0)
                    {
                        var newPaper = httpResponseMessage.ToList();

                    }
                }
            }
            catch (Exception ex) { }
            return null;

        }

        [Route("SavePaperData")]
        [HttpPost]
        [ActionName("SavePaperData")]
        public async Task<Paper> SaveFileData()
        {
            try
            {
                Paper paper = new Paper();
                paper.FileName = HttpContext.Request.Form["FileName"];
                paper.Name = HttpContext.Request.Form["Name"];
                paper.AllocatedTimeInMinutes = Convert.ToInt32(HttpContext.Request.Form["AllocatedTimeInMinutes"]);
                paper.Description = HttpContext.Request.Form["Description"];
                paper.NoOfQuestions = Convert.ToInt32(HttpContext.Request.Form["NoOfQuestions"]);
                paper.QualificationRevisionId = Convert.ToInt32(HttpContext.Request.Form["QualificationRevisionId"]);
                paper.QualificationStageTermId = Convert.ToInt32(HttpContext.Request.Form["QualificationStageTermId"]);
                paper.QualificationSubjectId = Convert.ToInt32(HttpContext.Request.Form["QualificationSubjectId"]);
                // paper.UploadingPaper = CHttpContext.Request.Form.Files[0];
                paper.Year = Convert.ToInt32(HttpContext.Request.Form["Year"]);

                var files = Request.Form.Files;
                foreach (var file in files)
                {
                    using (var ms = new MemoryStream())
                    {
                        file.CopyTo(ms);
                        paper.UploadingPaper = ms.ToArray();
                    }
                }


                using (var content = new MultipartFormDataContent())
                {
                    var values = new[]
                    {
                           new KeyValuePair<string, string>("Name",  paper.Name),
                  new KeyValuePair<string, string>("AllocatedTimeInMinutes",  paper.AllocatedTimeInMinutes.ToString()),
                   new KeyValuePair<string, string>("Description",  paper.Description),
                   new KeyValuePair<string, string>("NoOfQuestions",  paper.NoOfQuestions.ToString()),
                  new KeyValuePair<string, string>("QualificationRevisionId",  paper.QualificationRevisionId.ToString()),
                   new KeyValuePair<string, string>("QualificationStageTermId",  paper.QualificationStageTermId.ToString()),
                   new KeyValuePair<string, string>("QualificationSubjectId",  paper.QualificationSubjectId.ToString()),
                   new KeyValuePair<string, string>("Year",  paper.Year.ToString()),
                  // new KeyValuePair<string, string>("UploadingPaper",  paper.UploadingPaper.ToString()),
                        };

                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }

                    var fileContent = new ByteArrayContent(paper.UploadingPaper);

                    content.Add(fileContent, "UploadingPaper", paper.FileName);

                    _client.Client.DefaultRequestHeaders.Accept.Clear();
                    _client.Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string token = GetAccesssToken();
                    if (!string.IsNullOrEmpty(token))
                    {
                        _client.Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                        var httpResponseMessage = await _client.Client.PostAsync(string.Format("{0}/api/{1}/papers", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion), content);

                        if (httpResponseMessage.IsSuccessStatusCode)
                        {
                            var newPaper = await httpResponseMessage.Content.ReadAsAsync<Paper>();
                            return newPaper;
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return null;
        }
    }

}
