﻿using Microsoft.AspNetCore.Mvc;
using OnlineLearningSolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace OnlineLearningSolution.ApiController
{
    public class ApiContollerBase : ControllerBase
    {        
        protected  string GetAccesssToken()
        {
            return HttpContext.User.Claims.FirstOrDefault(x => x.Type == "AccessToken")?.Value;
        }

        protected async Task<bool> UpdateAccessToken(LMSHttpServiceClinet clinet, string apiVersion)
        {
            string refreshToken = HttpContext.User.Claims.FirstOrDefault(x => x.Type == "RefreshToken")?.Value;
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("RefreshToken", refreshToken));
            values.Add(new KeyValuePair<string, string>("AccessToken", GetAccesssToken()));
         
                var content = new FormUrlEncodedContent(values);
              var  httpResponseMessage = await clinet.Client.PostAsync(string.Format("/quizapp/api/{0}/user_logins/sign_in", apiVersion), content);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var userLogin = await httpResponseMessage.Content.ReadAsAsync<UserLogin>();
//HttpContext.User.Claims.
                return true;
            }
            return false;
        }
    }
}
