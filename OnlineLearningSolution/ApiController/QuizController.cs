﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OnlineLearningSolution.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineLearningSolution.ApiController
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class QuizController : ApiContollerBase
    {
        private readonly LMSHttpServiceClinet _client;
        private readonly IOptions<ElearningService> _appSettings;

        public QuizController(IOptions<ElearningService> appSettings, LMSHttpServiceClinet clinet)
        {
            _client = clinet;
            _appSettings = appSettings;
        }

        [Route("GetQuizQuestionsById")]
        [HttpGet]
        public async Task<List<PaperQuestionModel>> GetQuizQuestionsById(int quizId)
        {
            string token = GetAccesssToken();
            var response = await _client.GetAsync<List<PaperQuestionModel>>(string.Format("{0}/api/{1}/paper_questions?paperId={2}", _appSettings.Value.PathBase, _appSettings.Value.ApiVersion, quizId), token);

            return response.ToList();
        }

       
    }
}
