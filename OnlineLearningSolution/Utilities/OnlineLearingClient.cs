﻿using Newtonsoft.Json;
using OnlineLearningSolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLearningSolution
{
     public class OnlineLearingClient
    {
        
        public HttpClient Client { get; private set; }

        public OnlineLearingClient(HttpClient _client)
        {
            Client = _client;            
        }
 
    }

}
