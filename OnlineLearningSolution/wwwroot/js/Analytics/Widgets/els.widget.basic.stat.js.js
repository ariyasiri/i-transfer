﻿
els.widgets.basicStat = {};

(function (context) {
    var layoutId = "Layout1";
    var $pgrStatGrid = null, $speedGapChart = null, $maturityGainChart = null, $gaugeSpeed = null, $gaugeMarks = null;
    var colorMaturity = "#ba5fd4ba", colorAvgMaturity = "#3aaa3a", colorTrgMaturity = "#5fd4a1bf";
    var colorSpeed = "#5fb8d4", colorAvgSpeed = "#3aaa3a", colorTrgSpeed = "#a93aaa";

    var avgSpeed = 83, marks = 60;

    var timeSpendData = {correct :70, incorrect:30};

    var qstAnsList = [{ result: 1,  qestionNo: 1, id: 2, expTime: 70, actTIme: 80, accuracy: 1, expSpeed: 0.0142857142857143, actSpeed: 0.0125, speedDiff: -0.00178571428571428 },
        { result: 1, qestionNo: 2, id: 20, expTime: 50, actTIme: 75, accuracy: 0, expSpeed: 0.02, actSpeed: 0.0133333333333333, speedDiff: -0.00666666666666667, },
        { result: 2, qestionNo: 3, id: 150, expTime: 30, actTIme: 45, accuracy: 1, expSpeed: 0.0333333333333333, actSpeed: 0.0222222222222222, speedDiff: -0.0111111111111111, },
        { result: 1, qestionNo: 4, id: 15, expTime: 40, actTIme: 50, accuracy: 0, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
        { result: 1, qestionNo: 5, id: 157, expTime: 48, actTIme: 52, accuracy: 1, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 1 , qestionNo: 6, id: 78, expTime: 90, actTIme: 60, accuracy: 0, expSpeed: 0.0111111111111111, actSpeed: 0.0166666666666667, speedDiff: 0.00555555555555556, },
    {result : 2 , qestionNo: 7, id: 54, expTime: 120, actTIme: 135, accuracy: 1, expSpeed: 0.00833333333333333, actSpeed: 0.00740740740740741, speedDiff: -0.000925925925925926, },
    {result : 1 , qestionNo: 8, id: 9, expTimeexpTime: 100, actTIme: 80, accuracy: 0, expSpeed: 0.01, actSpeed: 0.0125, speedDiff: 0.0025, },
    {result : 1 , qestionNo: 9, id: 63, expTime: 60, actTIme: 70, accuracy: 1, expSpeed: 0.0166666666666667, actSpeed: 0.0142857142857143, speedDiff: -0.00238095238095238, },
    {result : 2 , qestionNo: 10, id: 28, expTime: 80, actTImeactTIme: 140, accuracy: 1, expSpeed: 0.0125, actSpeed: 0.00714285714285714, speedDiff: -0.00535714285714286, },
    {result : 1 , qestionNo: 11, id: 15, expTime: 40, actTIme: 50, accuracy: 0, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 1 , qestionNo: 12, id: 157, expTime: 48, actTIme: 52, accuracy: 1, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 1 , qestionNo: 13, id: 78, expTime: 90, actTIme: 60, accuracy: 0, expSpeed: 0.0111111111111111, actSpeed: 0.0166666666666667, speedDiff: 0.00555555555555556, },
    {result : 1 , qestionNo: 14, id: 54, expTime: 120, actTIme: 135, accuracy: 1, expSpeed: 0.00833333333333333, actSpeed: 0.00740740740740741, speedDiff: -0.000925925925925926, },
    {result : 1 , qestionNo: 15, id: 157, expTime: 48, actTIme: 52, accuracy: 1, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 1 , qestionNo: 16, id: 78, expTime: 90, actTIme: 60, accuracy: 0, expSpeed: 0.0111111111111111, actSpeed: 0.0166666666666667, speedDiff: 0.00555555555555556, },
    {result : 2 , qestionNo: 17, id: 54, expTime: 120, actTIme: 135, accuracy: 1, expSpeed: 0.00833333333333333, actSpeed: 0.00740740740740741, speedDiff: -0.000925925925925926, },
    {result : 1 , qestionNo: 18, id: 9, expTimeexpTime: 100, actTIme: 80, accuracy: 0, expSpeed: 0.01, actSpeed: 0.0125, speedDiff: 0.0025, },
    {result : 1 , qestionNo: 19, id: 63, expTime: 60, actTIme: 70, accuracy: 1, expSpeed: 0.0166666666666667, actSpeed: 0.0142857142857143, speedDiff: -0.00238095238095238, },
    {result : 2 , qestionNo: 20, id: 28, expTime: 80, actTImeactTIme: 140, accuracy: 1, expSpeed: 0.0125, actSpeed: 0.00714285714285714, speedDiff: -0.00535714285714286, },
    {result : 1 , qestionNo: 21, id: 15, expTime: 40, actTIme: 50, accuracy: 0, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 1 , qestionNo: 22, id: 157, expTime: 48, actTIme: 52, accuracy: 1, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 0 , qestionNo: 23, id: 78, expTime: 90, actTIme: 60, accuracy: 0, expSpeed: 0.0111111111111111, actSpeed: 0.0166666666666667, speedDiff: 0.00555555555555556, },
    {result : 1 , qestionNo: 24, id: 54, expTime: 120, actTIme: 135, accuracy: 1, expSpeed: 0.00833333333333333, actSpeed: 0.00740740740740741, speedDiff: -0.000925925925925926, },
    {result : 1 , qestionNo: 25, id: 9, expTimeexpTime: 100, actTIme: 80, accuracy: 0, expSpeed: 0.01, actSpeed: 0.0125, speedDiff: 0.0025, },
    {result : 1 , qestionNo: 26, id: 63, expTime: 60, actTIme: 70, accuracy: 1, expSpeed: 0.0166666666666667, actSpeed: 0.0142857142857143, speedDiff: -0.00238095238095238, },
    {result : 1 , qestionNo: 27, id: 54, expTime: 120, actTIme: 135, accuracy: 1, expSpeed: 0.00833333333333333, actSpeed: 0.00740740740740741, speedDiff: -0.000925925925925926, },
    {result : 1 , qestionNo: 28, id: 9, expTimeexpTime: 100, actTIme: 80, accuracy: 0, expSpeed: 0.01, actSpeed: 0.0125, speedDiff: 0.0025, },
    {result : 1 , qestionNo: 29, id: 63, expTime: 60, actTIme: 70, accuracy: 1, expSpeed: 0.0166666666666667, actSpeed: 0.0142857142857143, speedDiff: -0.00238095238095238, },
    {result : 1 , qestionNo: 30, id: 28, expTime: 80, actTImeactTIme: 140, accuracy: 1, expSpeed: 0.0125, actSpeed: 0.00714285714285714, speedDiff: -0.00535714285714286, },
    {result : 2 , qestionNo: 31, id: 15, expTime: 40, actTIme: 50, accuracy: 0, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 2 , qestionNo: 32, id: 157, expTime: 48, actTIme: 52, accuracy: 1, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 1 , qestionNo: 33, id: 78, expTime: 90, actTIme: 60, accuracy: 0, expSpeed: 0.0111111111111111, actSpeed: 0.0166666666666667, speedDiff: 0.00555555555555556, },
    {result : 1 , qestionNo: 34, id: 54, expTime: 120, actTIme: 135, accuracy: 1, expSpeed: 0.00833333333333333, actSpeed: 0.00740740740740741, speedDiff: -0.000925925925925926, },
    {result : 1 , qestionNo: 35, id: 157, expTime: 48, actTIme: 52, accuracy: 1, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 1 , qestionNo: 66, id: 78, expTime: 90, actTIme: 60, accuracy: 0, expSpeed: 0.0111111111111111, actSpeed: 0.0166666666666667, speedDiff: 0.00555555555555556, },
    {result : 1 , qestionNo: 37, id: 54, expTime: 120, actTIme: 135, accuracy: 1, expSpeed: 0.00833333333333333, actSpeed: 0.00740740740740741, speedDiff: -0.000925925925925926, },
    {result : 1 , qestionNo: 38, id: 9, expTimeexpTime: 100, actTIme: 80, accuracy: 0, expSpeed: 0.01, actSpeed: 0.0125, speedDiff: 0.0025, },
    {result : 1 , qestionNo: 39, id: 63, expTime: 60, actTIme: 70, accuracy: 1, expSpeed: 0.0166666666666667, actSpeed: 0.0142857142857143, speedDiff: -0.00238095238095238, },
    {result : 1 , qestionNo: 40, id: 28, expTime: 80, actTImeactTIme: 140, accuracy: 1, expSpeed: 0.0125, actSpeed: 0.00714285714285714, speedDiff: -0.00535714285714286, },
    {result : 1 , qestionNo: 41, id: 15, expTime: 40, actTIme: 50, accuracy: 0, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 1 , qestionNo: 42, id: 157, expTime: 48, actTIme: 52, accuracy: 1, expSpeed: 0.025, actSpeed: 0.02, speedDiff: -0.005 },
    {result : 1 , qestionNo: 43, id: 78, expTime: 90, actTIme: 60, accuracy: 0, expSpeed: 0.0111111111111111, actSpeed: 0.0166666666666667, speedDiff: 0.00555555555555556, },
    {result : 2 , qestionNo: 44, id: 54, expTime: 120, actTIme: 135, accuracy: 1, expSpeed: 0.00833333333333333, actSpeed: 0.00740740740740741, speedDiff: -0.000925925925925926, },
    {result : 1 , qestionNo: 45, id: 9, expTimeexpTime: 100, actTIme: 80, accuracy: 0, expSpeed: 0.01, actSpeed: 0.0125, speedDiff: 0.0025, },
    {result : 2  , qestionNo: 46, id: 63, expTime: 60, actTIme: 70, accuracy: 1, expSpeed: 0.0166666666666667, actSpeed: 0.0142857142857143, speedDiff: -0.00238095238095238, }];

    context.init = function (options) {
     
        layoutId = options.layoutId;
        controlId = createContentUUID();
        $("#" + layoutId).empty().append(context.createHTMLContent());
        context.createBsStatGrid();
        context.createSpeedGapChart();
        context.createMaturityGainChart();    
        context.createGaugeMark();
        context.createGaugeAvgSpeed();
        context.createTimeDistributionChart();
    };

    context.createBsStatGrid = function () {
        var qtnAnsData = [];
        for (var i = 0; i < qstAnsList.length; i++) {
            if (qstAnsList[i].result == 1)
                qstAnsList[i]["pass"] = 0;
            if (qstAnsList[i].result == 2)
                qstAnsList[i]["fail"] = 0;
        }

        var at = {};// at["name"] = "Question Number"; for (var i = 0; i < qstAnsList.length; i++) { at["column" + i] = qstAnsList[i].qestionNo; } qtnAnsData.push(at);
        at = {}; at["name"] = "Exp Time"; for (var i = 0; i < qstAnsList.length; i++) { at["column" + i] = qstAnsList[i].expTime; } qtnAnsData.push(at);
        at = {}; at["name"] = "Act Time"; for (var i = 0; i < qstAnsList.length; i++) { at["column" + i] = qstAnsList[i].actTIme; } qtnAnsData.push(at);
        at = {}; at["name"] = "Accuracy"; for (var i = 0; i < qstAnsList.length; i++) { at["column" + i] = qstAnsList[i].accuracy; } qtnAnsData.push(at);
        at = {}; at["name"] = "Exp Speed"; for (var i = 0; i < qstAnsList.length; i++) { at["column" + i] = qstAnsList[i].expSpeed; } qtnAnsData.push(at);
        at = {}; at["name"] = "Act Speed"; for (var i = 0; i < qstAnsList.length; i++) { at["column" + i] = qstAnsList[i].actSpeed; } qtnAnsData.push(at);
        at = {}; at["name"] = "Speed Diff"; for (var i = 0; i < qstAnsList.length; i++) { at["column" + i] = qstAnsList[i].speedDiff; } qtnAnsData.push(at);
 
        var columnList = [];
        columnList.push({ field: "name", title: "Question Number", width:100 });
        for (var i = 0; i < qstAnsList.length; i++) {
            var col = {};
            col["field"] = "column" + i;
            col["title"] = qstAnsList[i].qestionNo.toString();
            col["width"] = 100,
            col["attributes"] = { style: "text-align: right; font-size: 12px" };
            columnList.push(col);
        }
        $pgrStatGrid = $("#gridBasicStat" + controlId).kendoGrid({
            dataSource: {
                data: qtnAnsData
            },
            columns: columnList
        }).data("kendoGrid");
    };

    context.createSpeedGapChart = function () {
        $speedGapChart = $("#chartBsStSpeedGain" + controlId).kendoChart({
            dataSource: {
                data: qstAnsList
            },
            theme:"metro",
            title: {
                text: "Question wise answering speed gap"
            },
                legend: {
                position: "bottom",
                visible: true
            },
            seriesDefaults: {
                labels: {
                    visible: false,
                    background: "transparent"
                }
            },
            series: [{
                type: "column",
                style: "smooth",
                color: colorSpeed,
                field: "speedDiff",
                name: "Speed Gap",
                tooltip: {
                    visible: true,
                     format: "{0:N4}",
                    background: "white",
                    border: {
                        color: colorSpeed,
                        width: 1
                    }

                }
            },
                {
                    type: "line",
                    style: "smooth",
                    color: "#0cab0c",
                    field: "pass",
                    name: "Correct",
                    width: 0,
                    markers: {
                        visible: true,
                        background: "#0cab0c"
                    }
                },
                {
                type: "line",
                style: "smooth",
                    color: "red",
                field: "fail",
                    name: "Incorrect",
                    markers: {
                        visible: true,
                        background: "red"
                    },
                  width: 0
            }],
            valueAxis: [{
                majorGridLines: {
                    visible: true
                },
                title: { text: "Speed Gap" },
                visible: true
            }],
            categoryAxis: {
                field: "qestionNo",
                majorGridLines: {
                    visible: true
                },
                line: {
                    visible: false
                },
                title: { text: "Question Number" },
                labels: {
                    padding: { top: 155 }
                }
            },
            attributes: { style: "margin:0 auto;border:none; width: 100%;" }
        }).data("kendoChart");
    };

    context.createMaturityGainChart = function () {
        $maturityGainChart = $("#chartBsStMaturityGain" + controlId).kendoChart({
            dataSource: {
                data: qstAnsList
            },
            theme: "metro",
            title: {
                text: "Answering speed comparison"},
            legend: {
                position: "bottom",
                visible: true
            },
            seriesDefaults: {
                labels: {
                    visible: false,
                    background: "transparent"
                }
            },
            series: [{
                type: "line",
                //style: "smooth",
                color: colorMaturity,
                name: "Act Speed",
                field: "actSpeed",
                markers: {
                    visible: false,
                    background: colorMaturity
                },
                tooltip: {
                    visible: false,
                    format: "{0:N4}",
                    background: "white",
                    border: {
                        color: colorMaturity,
                        width: 1
                    }

                }
            }, {
                    type: "column",
                  //  style: "smooth",
                    color: colorTrgMaturity,
                    name: "Exp Speed",
                    field: "expSpeed",
                    tooltip: {
                        visible: false,
                        format: "{0:N4}",
                        background: "white",
                        border: {
                            width: 1,
                            color: colorTrgMaturity
                        }
                    }
                },
                {
                    type: "line",
                    style: "smooth",
                    color: "#0cab0c",
                    field: "pass",
                    name: "Correct",
                    width: 0,
                    tooltip: {
                        visible: false,
                        //  format: "{0:N4}",
                        background: "white",
                        border: {
                            color: colorSpeed,
                            width: 1
                        }

                    },
                    markers: {
                        visible: true,
                        background: "#0cab0c"
                    }
                },
                {
                    type: "line",
                    style: "smooth",
                    color: "red",
                    field: "fail",
                    name: "Incorrect",
                    width: 0,
                    tooltip: {
                        visible: false,
                        //  format: "{0:N4}",
                        background: "white",
                        border: {
                            color: colorSpeed,
                            width: 1
                        }

                    },
                    markers: {
                        visible: true,
                        background: "red"
                    }
                }
            ],
            tooltip: {
                visible: false,
            },
            valueAxis: [{
                majorGridLines: {
                    visible: true
                },
                title: { text: "Answering Speed" },
                visible: true
            }],
            categoryAxis: {
                field: "qestionNo",
                majorGridLines: {
                    visible: true
                },
                line: {
                    visible: false
                },
                title: { text: "Question Number" }
            },
            attributes: { style: "margin:0 auto;border:none; width: 100%;" }
        }).data("kendoChart");
    };

    context.createTimeDistributionChart = function () {
        $maturityGainChart = $("#chartBsStTimeDif" + controlId).kendoChart({  
            title: "Distribution of Time spent",
            legend: {
                position: "bottom",
                visible: true
            },
            theme: "metro",
            seriesDefaults: {
                labels: {
                    visible: false,
                    background: "transparent"
                }
            },
            height: 200,
            labels: {
                visible: true,
                background: "transparent"
            },
            series: [
                {
                    type: "bar",
                    stack: {
                        type: "100%"
                    }, name: "Correct", data: [timeSpendData.correct], visible: true, color: "green"
                },
                {
                    type: "bar",
                    stack: {
                        type: "100%"
                    }, name: "Incorrect", data: [timeSpendData.incorrect], visible: true, color: "red"
                }
            ] 
        }).data("kendoChart");
    };

    context.createGaugeMark = function () {
        
        $gaugeMarks= $("#gaugeBsStMarks" + controlId ).kendoRadialGauge({
            pointer: [{
                value: marks,
                color: "#7fc6dd",
                cap: { size: 0.15 },
                length: 0.5
            }],
            scale: {
                minorUnit: 5,
                startAngle: -30,
                endAngle: 210,
                max: 100
            }
        }).data("kendoRadialGauge");
    };

    context.createGaugeAvgSpeed = function () {
        
        $gaugeSpeed = $("#gaugeBsStAvgSpeed" + controlId).kendoRadialGauge({
            pointer: [{
                value: avgSpeed,
                color: "#7fc6dd",
                cap: { size: 0.15 },
                length: 0.5
            }],
            scale: {
                minorUnit: 5,
                startAngle: -30,
                endAngle: 210,
                max: 100
            }
        }).data("kendoRadialGauge");
    };
     
    $(window).resize(function () {
        $maturityGainChart.resize();
        $speedGapChart.resize();
    });

    context.createHTMLContent = function () {
        var arrayHtmL = [];
        arrayHtmL.push(
            '<div class="container-fluid">',
            '<div class="row" style="display:none">',
            '<div class="col-md-12 ">', '<div class="content-out">',  '<div class="content-in">',
            '<div style="font: 16px Arial, Helvetica, sans-serif; text-align:center; margin-bottom:8px;"> Question wise time variation </div> <div id="gridBasicStat' + controlId + '"></div>',
            '</div>', '</div > ', '</div>',
            '</div>',
            '<div class="row">',
            '<div class="col-md-12 ">', '<div class="content-out">',  '<div class="content-in">',
            '<div class="analytic-dashboard-chart1" id="chartBsStSpeedGain' + controlId + '"></div>',
            '</div>', '</div > ', '</div>', 
            '</div>',
               '<div class="row">',
            '<div class="col-md-12">',  '<div class="content-out">',  '<div class="content-in">',
            '<div class="analytic-dashboard-chart1" id="chartBsStMaturityGain' + controlId + '"></div>',
            '</div>','</div > ', '</div>',
            '</div>',
             '<div class="row">',
            '<div class="col-md-4">', '<div class="content-out">', '<div class="content-in">',
             '<div style="text-align:center">  Average Speed </div>',
            '<div class="analytic-dashboard-gauge-container" >',           
            '<div class="analytic-dashboard-gauge" id="gaugeBsStAvgSpeed' + controlId + '"></div>',
            '</div>', '</div > ', '</div>',
            '</div>',
             '<div class="col-md-4">',  '<div class="content-out">',  '<div class="content-in">',
            '<div class="analytic-dashboard-chart" style="height: 310px; !important" id="chartBsStTimeDif' + controlId + '"></div>',
            '</div>','</div > ', '</div>',
            '<div class="col-md-4">',  '<div class="content-out">',  '<div class="content-in">',
             '<div style="text-align:center">  Marks </div>',
                '<div class="analytic-dashboard-gauge-container" >',
            '<div class="analytic-dashboard-gauge" id="gaugeBsStMarks' + controlId + '"></div>',
            '</div>','</div > ', '</div>',
             '</div>',
            '</div>',
            '</div>'

        );
        return arrayHtmL.join('');
    };

})(els.widgets.basicStat);
