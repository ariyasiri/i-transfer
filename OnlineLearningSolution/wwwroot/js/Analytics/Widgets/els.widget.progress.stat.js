﻿ 
els.widgets.progresStat = {};
 
(function (context) {
    var layoutId = "Layout1";
    var $pgrStatGrid = null, $speedGainChart = null, $maturityGainChart = null;
    var colorMaturity = "#5fb8d4", colorAvgMaturity = "#3aaa3a", colorTrgMaturity = "#a93aaa";
    var colorSpeed = "#5fb8d4", colorAvgSpeed = "#3aaa3a", colorTrgSpeed = "#a93aaa";

    var quizAttempts = [{
        attempt: 1,
        time: 65,
        accuracy: 0,
        speed: 0.0154,
        runAcrcy: 0,
        maturity: 0,
        speedTarget: 0.017,
        maturityTarget: 80,
        avgSpeed: 0.0126,
        avgMaturity: 45.4643
    },
        {
            attempt: 2,
            time: 83,
            accuracy: 1,
            speed: 0.0120,
            runAcrcy: 1,
            maturity: 50,
            speedTarget: 0.017,
            maturityTarget: 80,
            avgSpeed: 0.0126,
            avgMaturity: 45.4643
        },
        {
            attempt: 3,
            time: 108,
            accuracy: 0,
            speed: 0.0093,
            runAcrcy: 1,
            maturity: 33.33,
            speedTarget: 0.017,
            maturityTarget: 80,
            avgSpeed: 0.0126,
            avgMaturity: 45.4643
        },
        {
            attempt: 4,
            time: 103,
            accuracy: 0,
            speed: 0.0097,
            runAcrcy: 1,
            maturity: 25.00,
            speedTarget: 0.017,
            maturityTarget: 80,
            avgSpeed: 0.0126,
            avgMaturity: 45.4643
        },
        {
            attempt: 5,
            time: 73,
            accuracy: 1,
            speed: 0.0137,
            runAcrcy: 2,
            maturity: 40,
            speedTarget: 0.017,
            maturityTarget: 80,
            avgSpeed: 0.0126,
            avgMaturity: 45.4643
        },
        {
            attempt: 6,
            time: 75,
            accuracy: 1,
            speed: 0.0133,
            runAcrcy: 2,
            maturity: 40,
            speedTarget: 0.017,
            maturityTarget: 80,
            avgSpeed: 0.0126,
            avgMaturity: 45.4643
        },
        {
            attempt: 7,
            time: 78,
            accuracy: 1,
            speed: 0.0128,
            runAcrcy: 4,
            maturity: 57.14,
            speedTarget: 0.017,
            maturityTarget: 80,
            avgSpeed: 0.0126,
            avgMaturity: 45.4643
        },
        {
            attempt: 8,
            time: 76,
            accuracy: 1,
            speed: 0.0132,
            runAcrcy: 5,
            maturity: 62.50,
            speedTarget: 0.017,
            maturityTarget: 80,
            avgSpeed: 0.0126,
            avgMaturity: 45.4643
        },
        {
            attempt: 9,
            time: 78,
            accuracy: 1,
            speed: 0.0128,
            runAcrcy: 6,
            maturity: 66.67,
            speedTarget: 0.017,
            maturityTarget: 80,
            avgSpeed: 0.0126,
            avgMaturity: 45.4643
        },
        {
            attempt: 10,
            time: 75,
            accuracy: 1,
            speed: 0.0133,
            runAcrcy: 7,
            maturity: 70,
            speedTarget: 0.017,
            maturityTarget: 80,
            avgSpeed: 0.0126,
            avgMaturity: 45.4643
        }];

    context.init = function (options) {
        layoutId = options.layoutId;
        controlId = createContentUUID();
        $("#" + layoutId).empty().append(context.createHTMLContent());
        context.createPrgStatGrid();
        context.createSpeedGainChart();
        context.createMaturityGainChart();
    };

    context.createPrgStatGrid = function () {
        var quizAttemptData = [];
        var at = {}; //at["name"] = "Attempt"; for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].attempt; } quizAttemptData.push(at);
        at = {}; at["name"] = "Time"; at["precision"] = 0; for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].time;  } quizAttemptData.push(at);
        at = {}; at["name"] = "Accuracy"; at["precision"] = 0; for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].accuracy; } quizAttemptData.push(at);
        at = {}; at["name"] = "Speed"; at["precision"] = 0; for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].speed; } quizAttemptData.push(at);
        at = {}; at["name"] = "RunAcrcy"; at["precision"] = 4;  for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].runAcrcy; } quizAttemptData.push(at);
        at = {}; at["name"] = "Maturity"; at["precision"] = 2; for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].maturity; } quizAttemptData.push(at);
        at = {}; at["name"] = "SpeedTarget"; at["precision"] = 3; for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].speedTarget; } quizAttemptData.push(at);
        at = {}; at["name"] = "MaturityTarget"; at["precision"] = 0; for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].maturityTarget; } quizAttemptData.push(at);
        at = {}; at["name"] = "AvgSpeed"; at["precision"] = 4; for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].avgSpeed; } quizAttemptData.push(at);
        at = {}; at["name"] = "TimAvgSpeede"; at["precision"] = 4; for (var i = 0; i < quizAttempts.length; i++) { at["column" + i] = quizAttempts[i].avgMaturity; } quizAttemptData.push(at);

        console.log(quizAttemptData);
        var columnList = [];
        columnList.push({ field: "name", title: "Attempt" });
        for (var i = 0; i < quizAttempts.length; i++) {
            var col = {};
            col["field"] = "column" + i;
           // col["template"] = function (data) {return kendo.toString(data.num, "n" + data.precision);};
            col["title"] = quizAttempts[i].attempt.toString();
            col["attributes"] = { style: "text-align: right; font-size: 12px" };
            console.log(JSON.stringify( col));
            columnList.push(col);
        }
        //columnList.push({
        //    field: "precision", visible: false
        //});
 
        $pgrStatGrid = $("#gridProgressStat" + controlId).kendoGrid({
            dataSource: {
                data: quizAttemptData
            },
            columns: columnList
        }).data("kendoGrid");
    };

    context.createSpeedGainChart = function () {
        $speedGainChart = $("#chartPrgStSpeedGain" + controlId).kendoChart({
                dataSource: {
                    data: quizAttempts
                },
                legend: {
                    position: "bottom",
                    visible: true
                },
                seriesDefaults: {
                    labels: {
                        visible: false,
                        background: "transparent"
                    }
                },
                series: [{
                    type: "line",
                    style: "smooth",
                    color: colorSpeed,
                    field: "speed",
                    name: "Speed",
                    tooltip: {
                        visible: false,
                      //  format: "{0:N4}",
                        background: "white",
                        border: {
                            color: colorSpeed,
                            width: 1
                        }

                    }
                }, {
                    type: "line",
                    style: "smooth",
                    color: colorTrgSpeed,
                        field: "speedTarget",
                        name: "Speed Target ",
                    tooltip: {
                        visible: false,
                        format: "{0:N4}",
                        background: "white",
                        border: {
                            width: 1,
                            color: colorTrgSpeed
                        }
                    }
                }, {
                    type: "line",
                    style: "smooth",
                    color: colorAvgMaturity,
                        field: "avgSpeed",
                        name: "Avg Speed",
                    tooltip: {
                        visible: false,
                        //format: "{0:N4}",
                        background: "white",
                        border: {
                            width: 1,
                            color: colorAvgSpeed
                        }
                    }
                }
                ],
                valueAxis: [{
                    majorGridLines: {
                        visible: true
                    },
                    title: { text: "Time" },
                    visible: true
                }],
                categoryAxis: {
                    field: "attempt",
                    majorGridLines: {
                        visible: true
                    },
                    line: {
                        visible: false
                    },
                    title: { text: "Attempt" }
                },
                attributes: { style: "margin:0 auto;border:none; width: 100%;" }
        }).data("kendoChart");
    };

    context.createMaturityGainChart = function () {
        $maturityGainChart = $("#chartPrgStMaturityGain" + controlId).kendoChart({
            dataSource: {
                data: quizAttempts
            },
            legend: {
                position: "bottom",
                visible: true
            },
            seriesDefaults: {
                labels: {
                    visible: false,
                    background: "transparent"
                }
            },
            series: [{
                type: "line",
                style: "smooth",
                color: colorMaturity,
                name: "Maturity",
                field: "maturity",
                tooltip: {
                    visible: false,
                    format: "{0:N4}",
                    background: "white",
                    border: {
                        color: colorMaturity,
                        width: 1
                    }

                }
            }, {
                    type: "line",
                    style: "smooth",
                color: colorTrgMaturity,
                    name: "Maturity Target",
                    field: "maturityTarget",
                    tooltip: {
                        visible: false,
                    format: "{0:N4}",
                    background: "white",
                    border: {
                        width: 1,
                        color: colorTrgMaturity
                    }
                }
                }, {
                    type: "line",
                    style: "smooth",
                    color: colorAvgMaturity,
                    name: "Avg Target",
                    field: "avgMaturity",
                    tooltip: {
                        visible: false,
                        format: "{0:N4}",
                        background: "white",
                        border: {
                            width: 1,
                            color: colorAvgMaturity
                        }
                    }
                }
            ],
            valueAxis: [{ 
                majorGridLines: {
                    visible: true
                },
                title: { text: "Time" },
                visible: true
            }],
            categoryAxis: {
                field: "attempt",
                majorGridLines: {
                    visible: true
                },
                line: {
                    visible: false
                },
                title: { text: "Attempt" }
            },
            attributes: { style: "margin:0 auto;border:none; width: 100%;" }
        }).data("kendoChart");
    };

    $(window).resize(function () {
       // $maturityGainChart.resize();
       // $speedGainChart.resize();
    });

    context.createHTMLContent = function () {
        var arrayHtmL = [];
        arrayHtmL.push(
            '<div class="container-fluid">',
            '<div class="row">',
            '<div class="col-md-12 ">',  '<div class="content-out">',  '<div class="content-in">',
            '<div style="font: 16px Arial, Helvetica, sans-serif; text-align:center; margin-bottom:8px;"> Question wise time to answer with acuracy percentage </div> <div id="gridProgressStat' + controlId + '"></div>',
            '</div>','</div>','</div>',
            '</div>',
            '<div class="row">',
            '<div class="col-md-6 ">',  '<div class="content-out">',  '<div class="content-in">',
            '<div style="font: 16px Arial, Helvetica, sans-serif; text-align:center; margin-bottom:8px;"> Speed Gain </div> <div class="analytic-dashboard-chart" id="chartPrgStSpeedGain' + controlId + '"></div>',
            '</div>','</div>','</div>',
            '<div class="col-md-6">',  '<div class="content-out">',  '<div class="content-in">',
            '<div style="font: 16px Arial, Helvetica, sans-serif; text-align:center; margin-bottom:8px;"> Maturity Gain </div> <div class="analytic-dashboard-chart" id="chartPrgStMaturityGain' + controlId + '"></div>',
            '</div>','</div>','</div>',
            '</div>',
            '</div>'
           
        );
        return arrayHtmL.join('');
    };
 
})(els.widgets.progresStat);
