﻿ 
els.modules.quiz.createquiz = {};

(function (context) {
    var qualificationId = null ,selectedSubjectId = null;
    var $ddlQualifications = null, $ddStage = null, $ddlRevison =null, $ddlTerm = null, $ddlUnit = null, $ddlSubject = null,  $dpYearFrom = null, $dpYearTo = null, $numAllctTime = null, $numNoOfQtn = null;
    var qualificationStageList = null, tempList = null, qualificationRevisionList = null, qualificationsList = null, unitList = null;
    var questionList = [], selQuestion = undefined, $numDiff0 = null, $numDiff1 = null, $numDiff2 = null;
    var indexop = 0, tmcountdown = 0, tmcountdownGap = 2000; // indexop ->this is for check idel

    context.init = function () { 
        context.setQuizDetails(0);

        $(window).focus(function () {
            indexop = 0;
        })
        .blur(function () {
                // indexop++;
         })
        .scroll(function () {
                indexop = 0;
            })
        .click(function () {
                indexop = 0;
         })
        .keyup(function () {
                indexop = 0;
        })
        .keydown(function () {
                indexop = 0;
         });

        $(document).mousemove(function (e) {
            indexop = 0;
        });

        $("#buttonCreateQuiz").click(function () {
            var obj = {
                qualifiacatioId:$ddlQualifications.value(),
                subject: $ddlSubject.value(),
                unitList: $ddlUnit.value(),
                stageId: $ddStage.value(),
                termId: $ddlTerm.value(),
                yearForm: $dpYearFrom._oldText,
                yearTo: $dpYearTo._oldText,
                type: $('input[name="radioQuizType"]:checked').val(),
                time: $numAllctTime.value(),
                noOfQuestions: $numNoOfQtn.value()
            };
            //need to add quiz saving function here

            $.ajax({
                type: "Get",
                url: baseUrl + "/api/admin/GetPaperQuestionById",
                data: { paperId: 3031},
                dataType: 'JSON',
                async: true,
                success: function (data) {
                    $("#createQuizMain").css("display", "none");
                    $("#qContentMain").css("display", "block");
                    questionList = data;
                    context.createPagination(1, questionList.length);
                    if (questionList != undefined)
                        context.findAndSetSelectedQuestion(1);
                    context.setTimeCoutdown();
                }
            });    
        });

        $("#buttonCreateQuizCancel").click(function () {

        });

        $("#bttnAnsSelctNGo").click(function () {            
            for (var i = 0; i < questionList.length; i++) {
                if (questionList[i].question.questionId == selQuestion.question.questionId) {
                    if ($('input[name="paperQtnAwr"]:checked').val() != undefined) {
 
                        if (($('input[name="paperQtnAwr"]:checked').val() != questionList[i].question.givenAns) || ((questionList[i].question.isNotSure == undefined) ? false :($("#paperQtnAwrSure").prop("checked") != questionList[i].question.isNotSure)))
                            selQuestion.question.isDirty = true;

                        questionList[i].question.givenAns = $('input[name="paperQtnAwr"]:checked').val();
                        questionList[i].question.isNotSure = $("#paperQtnAwrSure").prop("checked");  
 

                   }
                    if ((questionList.length - 1) >= i) {
                        var index = i + 1;
                        if ((questionList.length - 1) == i)
                            index = i;
                        $('#paperQuestionNavigation').pagination('selectPage', questionList[index].questionOrder);
                        context.findAndSetSelectedQuestion(questionList[index].questionOrder);
                    }
                    
                    break;
                }

            }
            
        });
    };

    context.setTimeCoutdown = function autoSave() {
        setInterval(function () { 
            tmcountdown++;
        }, tmcountdownGap);
    };

    context.setPageNavColor = function () {
 
        for (var i = 0; i < $('#paperQuestionNavigation li a').length; i++) {           
            for (var j = 0; j < questionList.length; j++) { 
                if (questionList[j].questionOrder == parseInt($($('#paperQuestionNavigation li a')[i]).text())) {
                    if (questionList[j].question.isNotSure)  
                        $($('#paperQuestionNavigation li a')[i]).addClass('nav-select-uncertain');
                    if (questionList[j].question.givenAns != undefined)  
                        $($('#paperQuestionNavigation li a')[i]).addClass('nav-select-success');
                    break;
                }
            }
        }

        for (var j = 0; j < questionList.length; j++) {
            if (questionList[j].questionOrder == $('#paperQuestionNavigation .nav-select-current ').text()) {
                if (questionList[j].question.isNotSure)
                    $('#paperQuestionNavigation .nav-select-current ').addClass('nav-select-uncertain');
                if (questionList[j].question.givenAns != undefined)
                    $('#paperQuestionNavigation .nav-select-current ').addClass('nav-select-success');
                break;
            }
        }

        try {
            $("#bttnAnsSelctNGo").val("Set Answer OR Skip & Next");
            if (questionList[questionList.length - 1].questionOrder == $('#paperQuestionNavigation .nav-select-current ').text())
                $("#bttnAnsSelctNGo").val("Set Answer OR Skip");
            }
        catch (e) { }

        try {
        var notAnsQst = $.grep(questionList, function (item) {
            return (item.question.givenAns == undefined || item.question.isNotSure == true);
        }).length;

            if (notAnsQst == 0) {
                if ($("#bttnQComplete").length == 0) {
                    $(".els-button-controller").append('<input type="button" value="Complete" id="bttnQComplete" class="common-button">');
                    context.completeQuiz();
                }
            }
            else
                $('#bttnQComplete').remove();
        }
        catch(e){ }
 
    };

    context.completeQuiz = function () {
        $("#bttnQComplete").unbind('click').bind('click', function () { alert('quiz Completed'); });
    }

    context.createPagination = function (questionno, noOfQuestions) {
        try { $('#paperQuestionNavigation').pagination('destroy'); } catch (e) { }
        $('#paperQuestionNavigation').pagination({
            pages: 1,
            cssStyle: 'light-theme',
            prevText: "<",
            nextText: ">",
            onPageClick: function (e) {
                context.findAndSetSelectedQuestion(e);
            }
        });

        $('#paperQuestionNavigation').pagination('updateItems', noOfQuestions);

        $('#paperQuestionNavigation').pagination('selectPage', questionno);
 
    };

    context.autoSaveAnswer = function () {

        if (selQuestion.question.isDirty || (tmcountdown * (tmcountdownGap / 1000) > 21)) {
            var att = {}; tmcountdown = 0;
            console.log('saving');
            try {
                selQuestion.question.isDirty ? wrapLMSContent("qContentMain") : "";
                var ajaxRequest = $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: baseUrl + "/api/admin/SaveQuestionData",
                    contentType: false,
                    processData: false,
                    aync: selQuestion.question.isDirty ? true : false,
                    data: att,
                    success: function (data) {
                        if ($(".temp-content").length > 0) unwrapLMSContent("qContentMain");

                    },
                    error: function (data) {
                        if ($(".temp-content").length > 0) unwrapLMSContent("qContentMain");
                    }
                });
            }
            catch (e) { if ($(".temp-content").length > 0) unwrapLMSContent("qContentMain");}
        }
    };

    context.findAndSetSelectedQuestion = function (questionno) {
        if (selQuestion != undefined) {
            for (var i = 0; i < questionList.length; i++) {
                if (questionList[i].question.questionId == selQuestion.question.questionId) {
                    questionList[i].question.timeTaken = (questionList[i].question.timeTaken != undefined) ? questionList[i].question.timeTaken += tmcountdown * (tmcountdownGap / 1000) : questionList[i].question.timeTaken = tmcountdown * (tmcountdownGap / 1000);
                    break;
                }
            }
            context.autoSaveAnswer(); tmcountdown = 0;
        }     

        selQuestion = undefined;
        for (var i = 0; i < questionList.length; i++) {
            if (questionList[i].questionOrder == questionno) {
                selQuestion = questionList[i];
                selQuestion.question.isDirty = false;
                break;
            }
        }

        $("#paperSelQtnContent").empty();
        if (selQuestion != undefined) { 
            context.setQuetionContent(selQuestion);        }
        else {
            $("#paperEditQuestion").css("display", "none");
            $("#paperSelQtnContent").append('<div style="text-align: center; height: 340px; object - fit: contain;"><strong>Question has not been added for selected question number.</strong></div>');
        } 
        context.setPageNavColor();  
    };

    context.setQuetionContent = function (selectedQtn) {
        $("#paperSelQtnContent").empty();
        $("#paperEditQuestion").css("display", "inline-block");

        $("#paperSelQtnContent").append('<div style="text-align:center; height:280px; "><img class="imageQContent" style="height:275px;object-fit: scale-down !important;" src="' + selectedQtn.question.questionImageLocation + '" /> </div>');
        var ansStrList = '<br/> <div class="els-radio-answer" >';
        for (i = 1; i <= selectedQtn.question.noOfAnswers; i++) {
            if (selectedQtn.question.givenAns == i)
                ansStrList += '<input type="radio" value="'+i+'" checked  name="paperQtnAwr" id="paperQtnAwr' + i + '" class="k-radio radio-quiz-answer"  >   <label class="k-radio-label" for="paperQtnAwr' + i + '">   &nbsp;' + i + '</label>';
            else
                ansStrList += '<input type="radio" value="' + i + '"   name="paperQtnAwr" id="paperQtnAwr' + i + '" class="k-radio radio-quiz-answer"  >   <label class="k-radio-label" for="paperQtnAwr' + i + '">   &nbsp;' + i + '</label>';
        }
        ansStrList += '</div> <br/>';
        $("#paperSelQtnContent").append(ansStrList);
        $("#imageQContent").css("object-fit", "scale-down !important");

        if (selectedQtn.question.isNotSure)
            $("#paperQtnAwrSure").prop("checked", true);
        else
            $("#paperQtnAwrSure").prop("checked", false);
       
    };
    
    context.setQuizDetails = function (quizId) {
        context.getQualifications();
        if ($ddlQualifications == null)
            $ddlQualifications = $("#ddlQalification").kendoDropDownList({
                dataTextField: "name",
                dataValueField: "id",
                dataSource: qualificationsList,
                index: 0,
                change: context.changeQualification
            }).data("kendoDropDownList");
        if (quizId == 0) {
            $ddlQualifications.select(0);
            context.setQualifsationChange();
        }

        if ($dpYearFrom == null)
            $dpYearFrom = $("#dateYearFrom").kendoDatePicker({
                start: "decade",
                depth: "decade",
                format: "yyyy",
                change: context.startChange
            }).data("kendoDatePicker");

        if ($dpYearTo == null)
            $dpYearTo = $("#dateYearTo").kendoDatePicker({
                start: "decade",
                depth: "decade",
                format: "yyyy",
                change: context.endChange
            }).data("kendoDatePicker");

      //  $dpYearFrom.max($dpYearTo.value());
        $dpYearTo.min($dpYearFrom.value());

        if ($ddlUnit == null)
            $ddlUnit = $("#multiselectAnsUnit").kendoMultiSelect({
                placeholder: "Select unit(s)...",
                dataTextField: "name",
                dataValueField: "unitId",
                autoBind: false,
                dataSource: []
            }).data("kendoMultiSelect");

        if ($numAllctTime == null)
            $numAllctTime = $("#numAllctTime").kendoNumericTextBox({ min: 1, format: "N0" }).data("kendoNumericTextBox");
        if ($numNoOfQtn == null)
            $numNoOfQtn = $("#numNoOfQstn").kendoNumericTextBox({ min: 1, format: "N0" }).data("kendoNumericTextBox");
        if ($numDiff0 == null)
            $numDiff0 = $("#diffLevel0").kendoNumericTextBox({ min: 1, format: "N0" }).data("kendoNumericTextBox");
        if ($numDiff1 == null)
            $numDiff1 = $("#diffLevel1").kendoNumericTextBox({ min: 1, format: "N0" }).data("kendoNumericTextBox");
        if ($numDiff2 == null)
            $numDiff2 = $("#diffLevel2").kendoNumericTextBox({ min: 1, format: "N0" }).data("kendoNumericTextBox");

     
    };

    context.startChange = function () {
        var startDate = $dpYearFrom.value(),
            endDate = $dpYearTo.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            $dpYearTo.min(startDate);
        } else if (endDate) {
           // $dpYearFrom.max(new Date(endDate));
        } else {
            endDate = new Date();
            //$dpYearFrom.max(endDate);
            $dpYearTo.min(endDate); 
        }

        if (startDate > endDate)
            $dpYearTo.value(startDate);
    };

    context.endChange = function () {
        //var endDate = $dpYearFrom.value(),
        //    startDate = $dpYearTo.value();

        //if (endDate) {
        //    endDate = new Date(endDate);
        //    endDate.setDate(endDate.getDate());
        //    $dpYearFrom.max(endDate);
        //} else if (startDate) {
        //    $dpYearTo.min(new Date(startDate));
        //} else {
        //    endDate = new Date();
        //    $dpYearFrom.max(endDate);
        //    $dpYearTo.min(endDate);
        //}
    };

    context.getQualifications = function () {
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetAllQualifications",
            dataType: 'JSON', async: false,
            success: function (data) {
                qualificationsList = data;
                qualificationId = data[0].id;
            }
        });
    };

    context.setQualifsationChange = function () { 
        context.getQualificationStages();
        context.getQualificationTerms();
        context.GetAllQualificationSubjects();
        context.getQualificationRevision();
    };

    context.getQualificationStages = function () {
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetQualificationStages",
            data: { qualificationId: qualificationId },
            dataType: 'JSON',
            async: true,
            success: function (data) {
                qualificationStageList = data;
                if ($ddStage == null)
                    $ddStage = $("#ddlStage").kendoDropDownList({
                        dataTextField: "stage",
                        dataValueField: "id",
                        dataSource: [],
                        //index: 0
                    }).data("kendoDropDownList");
 
                var tempList = qualificationStageList;
                tempList.unshift({ stage: "All Stages", id: 0 });
                $ddStage.dataSource.data(tempList);                
            }

        });
    };

    context.getSelectedSubjectUnit = function () {
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetUnitsBySubject",
            data: { subjectId: selectedSubjectId },
            dataType: 'JSON',
            async: true,
            success: function (data) {
                unitList = data;               
                $ddlUnit.dataSource.data(unitList);
            }
        });
    };

    context.getQualificationTerms = function () {
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetQualificationStageTerms",
            dataType: 'JSON',
            data: { qualificationId: qualificationId },
            async: true,
            success: function (data) {
                qualificationTermList = data;

                if ($ddlTerm == null)
                    $ddlTerm = $("#ddlTerm").kendoDropDownList({
                        dataTextField: "term",
                        dataValueField: "id",
                        dataSource: []
                        // index: 0
                    }).data("kendoDropDownList");
                var tempList = qualificationTermList;
                tempList.unshift({ term: "All Terms", id: 0 });
                $ddlTerm.dataSource.data(qualificationTermList);
                $ddlTerm.select(0);
            }
        });
    };

    context.GetAllQualificationSubjects = function () {
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetQualificationSubjects",
            dataType: 'JSON',
            data: { qualificationId: qualificationId },
            async: true,
            success: function (data) {
                qualificationSubjectList = data;
                if ($ddlSubject == null)
                    $ddlSubject = $("#ddlQlftnSuj").kendoDropDownList({
                        dataTextField: "name",
                        dataValueField: "qualificationSubjectId",
                        dataSource: [],
                        change: context.changeQualificationSubject
                        // index: 0
                    }).data("kendoDropDownList");
 
                $ddlSubject.dataSource.data(qualificationSubjectList);
                $ddlSubject.select(0);
                $ddlSubject.trigger('change');
            }
        });
    };

    context.changeQualificationSubject = function () {
        selectedSubjectId = this.dataItem().qualificationSubjectId;
        if (selectedSubjectId > 0) {
            context.getSelectedSubjectUnit();
        }
    };

    context.getQualificationRevision = function () {
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetQualificationRevisions",
            dataType: 'JSON',
            async: true,
            data: { qualificationId: qualificationId },
            success: function (data) {
                qualificationRevisionList = data;
                if ($ddlRevison == null)
                    $ddlRevison = $("#ddlRevision").kendoDropDownList({
                        dataTextField: "revision",
                        dataValueField: "qualificationRevisionId",
                        dataSource: []
                        //index: 0
                    }).data("kendoDropDownList");

                var tempList = qualificationRevisionList;
                tempList.unshift({ revision: "All Revisions", qualificationRevisionId: 0 });
                $ddlRevison.dataSource.data(tempList);
                $ddlRevison.select(0);
            }
        });
    };

})(els.modules.quiz.createquiz);

