﻿var usrFeedbacks = [], $kendoWindow = null, selectedToolId = 0, $dtCallRsdl= null, feedbackId = 0; toolFeedback = { Id: 0, Rating: 0, comment: '' };
 
function notifyFeedback(htmlcontent, istimeron, callback, close_callback, style, text) {

    var time = '9000';
    var $container = $('.notifyjs-corner');
    var icon = '<i class="fa fa-info-circle "></i>';

    //if (typeof style == 'undefined' ) style = 'warning'

    //var html = $('<div class="alert alert-' + style + '  hide">' + icon +  " " + text + '</div>');
    var html = $(htmlcontent);

    //this is for close button if needed
    //$('<a>',{
    //	text: '×',
    //	class: 'button close',
    //	style: 'padding-left: 10px;',
    //	href: '#',
    //	click: function(e){
    //		e.preventDefault()
    //		close_callback && close_callback()
    //		remove_notice()
    //	}
    //}).prependTo(html)

    $container.prepend(html)
    html.removeClass('hide').hide().fadeIn('slow');

    function remove_notice() {
        html.stop().fadeOut('slow').remove();
    }

    if (istimeron)
        var timer = setInterval(remove_notice, time);

    //$(html).hover(function () {
    //    clearInterval(timer);
    //}, function () {
    //    timer = setInterval(remove_notice, time);
    //});

    //remove notice by click
    //html.on('click', function () {
    //	clearInterval(timer)
    //	callback && callback()
    //	remove_notice()
    //});
}

function OperatorFeedback() {
    selectedToolId = $(this).data("toolid");
    if ($dtCallRsdl == null)  {
        $("#showFeedbackDailog").remove();

        var htmlArray = new Array();
        htmlArray.push('<div id="showFeedbackDailog"   >',
            '<div  >',
            '<div  >',
            '<div style="padding: 15px;">',
            '<div>Your feedback is important to enable us to continually improve customer relationship.Please enter comments below. </div>',
            '<br /> ',
            '<table> <tr> <td style="width:160px;"> Rate this call : </td> <td> ',
            '<div style="margin-left: -5px;" id="starsFeedback" data-rating="0"><span data-value="1" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size: 2.3em;color: #e4e4e4;cursor: pointer;padding-left: 6px;" aria-hidden="true"></span><span data-value="2" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size: 2.3em;color: #e4e4e4;cursor: pointer;padding-left: 6px;" aria-hidden="true"></span><span data-value="3" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size: 2.3em; color: rgb(228, 228, 228); cursor: pointer; padding-left: 6px;" aria-hidden="true"></span><span data-value="4" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size: 2.3em; color: rgb(228, 228, 228); cursor: pointer; padding-left: 6px;" aria-hidden="true"></span><span data-value="5" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size:2.3em; color:#e4e4e4; cursor:pointer; padding-left:6px; " aria-hidden="true"></span></div>',
            '</td> </tr>',
            '<tr> <td style="width:160px;">Call Reschedule : </td> <td> ',
            '<input style="width:200px;"     type="date" id="dtCallResdl" />',
            '</td> </tr>',
            '<tr> <td style="width:160px;">Reason for not pay: </td> <td> ',
            '<input type="radio"> Forgot &nbsp; <input type="radio"> Ignore &nbsp; <input type="radio"> Having hard time &nbsp;<input type="radio"> Unable to pay &nbsp;<input type="radio"> Not interested &nbsp;<input type="radio"> Other &nbsp;',
            '</td> </tr>',
            '<tr> <td> Your feedback : </td>',
            '<td> <textarea id="textareaUsrFdback" rows="4" cols="50" style="width:100%; margin-bottom:12px; margin-top:12px;"> </textarea> </td> </tr>',
            '</table>',
            '<br />',
            '<div style="visibility: hidden; text-align: center;color: #29d827;font-size: 13px;font-weight: 600;margin: px;padding-top: -3px;margin-top: -10px;" id="showNotification" > &nbsp; </div> ',
            '</div>',

            '<div style="padding: 15px; text-align: right; padding-top: 0px;">',
            '<button type="button" class="btn btn-primary" id="bttnFbSubmit" >Submit</button> &nbsp;',
            '<button type="button" class="btn btn-primary" id="bttnFbreset">Cancel</button> &nbsp;',
            '</div>',
            '</div>',
            '</div>',
            '</div>');

        $("#wndwUserFeedback").append(htmlArray.join(''));

       

        $("#bttnFbSubmit").unbind("click").bind("click", function () {
            $kendoWindow.close();
        });

        $("#bttnFbreset").unbind("click").bind("click", function () {
            $kendoWindow.close();

        });

        $("#starsFeedback").empty().removeAttr("data-rating");
        ratingFeedback("create", { value: 2 });
        $("#textareaUsrFdback").val();
    }
    
    if ($kendoWindow == null) {
        $kendoWindow = $("#wndwUserFeedback").kendoWindow({
            width: "750px",
            actions: ["Close"],
             modal: true,
            close: function (e) {
                $("#callDial2, #callDial3").css("display", "none");
                $("#callDial1").css("display", "block");
            },
            title: "Feedback"
        }).data("kendoWindow");
    }
    console.log('xx00'); 
  
        $dtCallRsdl = $("#dtCallResdl").kendoDateTimePicker({
            value: new Date()
        }).data("kendoDateTimePicker");
    

    $kendoWindow.center().open();
};

function getToolFeedback() {
    $.ajax({
        async: true,
        dataType: "json",
        url: "../api/feedback/getUserFeedbaksByToolId",
        data: { toolId: selectedToolId },
        success: function (data) {
            if (data != null) {
                toolFeedback = data;
                feedbackId = data.Id;
            }

            $("#starsFeedback").empty().removeAttr("data-rating");
            $("#starsFeedback").ratingFeedback("create", { value: toolFeedback.Rating });
            $("#textareaUsrFdback").val(toolFeedback.Comment);
        },
        error: function (er) { console.log(er); }
    });
}

function getToolAllFeedbacks() {
    $.ajax({
        async: true,
        dataType: "json",
        url: "../api/feedback/getAllFeedbaksByToolId",
        data: { toolId: selectedToolId },
        success: function (data) {
            usrFeedbacks = data;
            var i = 1;
            var iscolored = true;
            if (usrFeedbacks.length > 0) {
                try {
                    notifyFeedback(previousUserFeedback(usrFeedbacks[0], 0, iscolored), usrFeedbacks.length > 3 ? true : false);
                    $("#feeedbackRating0").ratingFeedback("create", { size: "1.3em", value: usrFeedbacks[0].Rating, isfixed: true });
                }
                catch (e) { }
            }
            setInterval(function () {
                if (usrFeedbacks.length > 0) {
                    try {
                        iscolored = !iscolored;
                        var html = previousUserFeedback(usrFeedbacks[i], i, iscolored);
                        notifyFeedback(html, usrFeedbacks.length > 3 ? true : false);
                        $("#feeedbackRating" + i).ratingFeedback("create", { size: "1.3em", value: usrFeedbacks[i].Rating, isfixed: true });
                        i++;
                        if (usrFeedbacks.length > 3)
                            (i == usrFeedbacks.length) ? i = 0 : i = i;
                    } catch (e) { }
                } else {
                    i = 0;
                }
            }, 3000);
        },
        error: function (er) { console.log(er); }
    });
}

function saveToolFeedbacks() {
    var userFeedback = { id: toolFeedback.Id, toolId: selectedToolId, rating: $("#starsFeedback").attr('data-rating'), comment: $("#textareaUsrFdback").val() };
    $.ajax({
        async: true,
        url: "../api/feedback/mergeFeedback",
        type: 'POST',
        data: JSON.stringify(userFeedback),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            console.log(data); console.log(data.status);
            if (data.status) {
                if (feedbackId == 0) {
                    // usrFeedbacks.push(data.feedback);
                    feedbackId = data.feedback.Id;
                    toolFeedback = data.feedback;
                }
                toolFeedback = data.feedback;
                setFeedbackAlert("WS", "Thanking for your feedback.");
            }
            else {
                setFeedbackAlert("WE", "Feedback saving error.");
            }

        },
        error: function (data) {
            console.log(data);
            result = false;
        }
    });
}

function previousUserFeedback(usrFeedback, arrindex, iscolored) {
    var html = "";
    (iscolored) ? html += '<div style="background: #f7f7f7; padding-top:5px; padding-bottom:5px;"> ' : html += '<div style=" padding-top:5px; padding-bottom:5px;" > ';
    html += '<div style="display: flex;"> <a ><span style="font-size: 15px !important;" class="glyphicon glyphicon-user"></span></a> &nbsp; <span> ' + usrFeedback.Name + '</span> &nbsp; </div>' +
        '<div style="display: flex; padding: 3px;"> &nbsp;<div id="feeedbackRating' + arrindex + '">     </div>  </div> ' +
        '<div>  <div style="margin-left:14px; margin-bottom:6px; font-family: cursive;" > ' + usrFeedback.Comment + ' </div> </div> ' +
        '<div> ';

    return html;
}

function setFeedbackAlert(alrtClass, alrtMssge) {

    if (timeOut != null) clearTimeout(timeOut);

    if (alrtClass == "AS") {
        $("#showNotification").css("color", "#29d827");
    }
    else if (alrtClass == "AI") {
        $("#showNotification").css("color", "#327731");
    }
    else if (alrtClass == "AW") {
        $("#showNotification").css("color", "#b5ca44");
    }
    else if (alrtClass == "AE") {
        $("#showNotification").css("color", "#da7b44");
    }
    $('#showNotification').text(alrtMssge); $("#showNotification").css("visibility", "visible");
    timeOut = setTimeout(function () { $("#showNotification").css("visibility", "hidden"); }, 3000);

}



 
function ratingFeedback (method, options) {
            method = method || 'create';
            // This is the easiest way to have default options.
            var settings = $.extend({
                // These are the defaults.
                limit: 5,
                value: 0,
                glyph: "glyphicon-star",
                coloroff: "#e4e4e4",
                coloron: "#FFEB3B",
                size: "2.3em",
                cursor: "pointer",
                onClick: function () { },
                endofarray: "idontmatter",
                padding: "6px",
                isfixed: false
            }, options);
            var style = "";
            style = style + "font-size:" + settings.size + "; ";
            style = style + "color:" + settings.coloroff + "; ";
            style = style + "cursor:" + settings.cursor + "; ";
    style = style + "padding-left:" + settings.padding + "; ";
 
         var content = $("#starsFeedback");


            if (method == 'create') {
                //content.html('');	//junk whatever was there

                //initialize the data-rating property
                content.each(function () {
                    attr = $(content).attr('data-rating');
                    if (attr === undefined || attr === false) { $(content).attr('data-rating', settings.value); }
                });

                //bolt in the glyphs
                for (var i = 0; i < settings.limit; i++) {
                    content.append('<span data-value="' + (i + 1) + '" class="ratingicon glyphicon ' + settings.glyph + '" style="' + style + '" aria-hidden="true"></span>');
                }

                //paint
               // content.each(function () { paint($(content)); });

            }
            if (method == 'set') {
                content.attr('data-rating', options);
                content.each(function () { paint($(content)); });
            }
            if (method == 'get') {
                return content.attr('data-rating');
            }
            //register the click events
            if (!settings.isfixed) {
                content.find("span.ratingicon").click(function () {
                    rating = $(content).attr('data-value');
                    $(content).parent().attr('data-rating', rating);
                    paint($(content).parent());
                    settings.onClick.call($(content).parent());
                });
            }
            function paint(div) {
                rating = parseInt(div.attr('data-rating'));
                div.find("input").val(rating);	//if there is an input in the div lets set it's value
                div.find("span.ratingicon").each(function () {	//now paint the stars

                    var rating = parseInt($(content).parent().attr('data-rating'));
                    var value = parseInt($(content).attr('data-value'));
                    if (value > rating) { $(content).css('color', settings.coloroff); $(content).addClass('glyphicon-star-empty'); }
                    else { $(content).css('color', settings.coloron); $(content).removeClass('glyphicon-star-empty'); }
                });
            }

        };
 