﻿var usrFeedbacks = [], selectedToolId = 0, feedbackId = 0; toolFeedback = { Id: 0, Rating: 0, comment: '' };

(function ($) {

    $.fn.ratingFeedback = function (method, options) {
        method = method || 'create';
        // This is the easiest way to have default options.
        var settings = $.extend({
            // These are the defaults.
            limit: 5,
            value: 0,
            glyph: "glyphicon-star",
            coloroff: "#e4e4e4",
            coloron: "#FFEB3B",
            size: "2.3em",
            cursor: "pointer",
            onClick: function () { },
            endofarray: "idontmatter",
            padding: "6px",
            isfixed: false
        }, options);
        var style = "";
        style = style + "font-size:" + settings.size + "; ";
        style = style + "color:" + settings.coloroff + "; ";
        style = style + "cursor:" + settings.cursor + "; ";
        style = style + "padding-left:" + settings.padding + "; ";



        if (method == 'create') {
            //this.html('');	//junk whatever was there

            //initialize the data-rating property
            this.each(function () {
                attr = $(this).attr('data-rating');
                if (attr === undefined || attr === false) { $(this).attr('data-rating', settings.value); }
            })

            //bolt in the glyphs
            for (var i = 0; i < settings.limit; i++) {
                this.append('<span data-value="' + (i + 1) + '" class="ratingicon glyphicon ' + settings.glyph + '" style="' + style + '" aria-hidden="true"></span>');
            }

            //paint
            this.each(function () { paint($(this)); });

        }
        if (method == 'set') {
            this.attr('data-rating', options);
            this.each(function () { paint($(this)); });
        }
        if (method == 'get') {
            return this.attr('data-rating');
        }
        //register the click events
        if (!settings.isfixed) {
            this.find("span.ratingicon").click(function () {
                rating = $(this).attr('data-value')
                $(this).parent().attr('data-rating', rating);
                paint($(this).parent());
                settings.onClick.call($(this).parent());
            })
        }
        function paint(div) {
            rating = parseInt(div.attr('data-rating'));
            div.find("input").val(rating);	//if there is an input in the div lets set it's value
            div.find("span.ratingicon").each(function () {	//now paint the stars

                var rating = parseInt($(this).parent().attr('data-rating'));
                var value = parseInt($(this).attr('data-value'));
                if (value > rating) { $(this).css('color', settings.coloroff); $(this).addClass('glyphicon-star-empty'); }
                else { $(this).css('color', settings.coloron); $(this).removeClass('glyphicon-star-empty'); }
            })
        }

    };

}(jQuery));

function notifyFeedback(htmlcontent, istimeron, callback, close_callback, style, text) {

    var time = '9000';
    var $container = $('.notifyjs-corner');
    var icon = '<i class="fa fa-info-circle "></i>';

    //if (typeof style == 'undefined' ) style = 'warning'

    //var html = $('<div class="alert alert-' + style + '  hide">' + icon +  " " + text + '</div>');
    var html = $(htmlcontent);

    //this is for close button if needed
    //$('<a>',{
    //	text: '×',
    //	class: 'button close',
    //	style: 'padding-left: 10px;',
    //	href: '#',
    //	click: function(e){
    //		e.preventDefault()
    //		close_callback && close_callback()
    //		remove_notice()
    //	}
    //}).prependTo(html)

    $container.prepend(html)
    html.removeClass('hide').hide().fadeIn('slow')

    function remove_notice() {
        html.stop().fadeOut('slow').remove()
    }

    if (istimeron)
        var timer = setInterval(remove_notice, time);

    //$(html).hover(function () {
    //    clearInterval(timer);
    //}, function () {
    //    timer = setInterval(remove_notice, time);
    //});

    //remove notice by click
    //html.on('click', function () {
    //	clearInterval(timer)
    //	callback && callback()
    //	remove_notice()
    //});
}

$(function () {
    $("#customerFeedback").unbind('click').bind('click', function (event) {
        event.preventDefault();
        selectedToolId = $(this).data("toolid");
        if (!$("#showFeedbackDailog").length) {
            //$("#showFeedbackDailog").remove();

            getToolFeedback();
            getToolAllFeedbacks();

            var htmlArray = new Array();
            htmlArray.push('<div id="showFeedbackDailog" class="modal fade" role="dialog" >',
                '<div class="modal-dialog modal-lg" style="width: 720px !important;">',
                '<div class="modal-content">',
                '<div class="modal-header" style="font-size:18px;"> Feedback',
                '<button type="button" class="close" data-dismiss="modal">&times;</button>',
                '</div>',
                '<div style="padding: 15px;">',
                '<div>Your feedback is important to enable us to continually improve each Commercial Analytics tool.Please share your comments below. </div>',
                '<br /> ',
                '<table> <tr> <td style="width:160px;"> Rate this tool : </td> <td> ',
                '<div style="margin-left: -5px;" id="starsFeedback" data-rating="0"><span data-value="1" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size: 2.3em;color: #e4e4e4;cursor: pointer;padding-left: 6px;" aria-hidden="true"></span><span data-value="2" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size: 2.3em;color: #e4e4e4;cursor: pointer;padding-left: 6px;" aria-hidden="true"></span><span data-value="3" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size: 2.3em; color: rgb(228, 228, 228); cursor: pointer; padding-left: 6px;" aria-hidden="true"></span><span data-value="4" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size: 2.3em; color: rgb(228, 228, 228); cursor: pointer; padding-left: 6px;" aria-hidden="true"></span><span data-value="5" class="ratingicon glyphicon glyphicon-star glyphicon-star-empty" style="font-size:2.3em; color:#e4e4e4; cursor:pointer; padding-left:6px; " aria-hidden="true"></span></div>',
                '</td> </tr>',
                '<tr> <td> Your feedback : </td>',
                '<td> <textarea id="textareaUsrFdback" rows="4" cols="50" style="width:100%; margin-bottom:12px; margin-top:12px;"> </textarea> </td> </tr>',
                '<tr> <td> Feedback from other users :</td>',
                '<td> <div style="left: 50px;width: 100%;height: 200px !important;overflow-y: auto; border: 1px solid #f7f7f7;">',
                '<div class="notifyjs-corner" style="top: 0px;right: 0px;margin: 5px;z-index: 1050;">  </td> </tr>',
                '</table>',
                '<br />',
                '<div style="visibility: hidden; text-align: center;color: #29d827;font-size: 13px;font-weight: 600;margin: px;padding-top: -3px;margin-top: -10px;" id="showNotification" > &nbsp; </div> ',
                '</div>',

                '<div style="padding: 15px; text-align: right; padding-top: 0px;">',
                '<button type="button" class="btn btn-primary" id="bttnFbSubmit" >Submit</button> &nbsp;',
                '<button type="button" class="btn btn-primary" id="bttnFbreset">Reset</button> &nbsp;',
                '</div>',
                '</div>',
                '</div>',
                '</div>');

            $("body").append(htmlArray.join(''));

            $("#bttnFbSubmit").unbind("click").bind("click", function () {
                saveToolFeedbacks();
            });

            $("#bttnFbreset").unbind("click").bind("click", function () {
                $("#starsFeedback").empty().removeAttr("data-rating");
                $("#starsFeedback").ratingFeedback("create", { value: toolFeedback.Rating });
                $("#textareaUsrFdback").val(toolFeedback.Comment);

            });

            $("#showFeedbackDailog .modal-header").attr("style", "background-color:" + $(".btn-primary").css("background-color") + " !important; font-size:18px;");
        }

        $("#showFeedbackDailog").modal('show');
    });
});

function getToolFeedback() {
    $.ajax({
        async: true,
        dataType: "json",
        url: "../api/feedback/getUserFeedbaksByToolId",
        data: { toolId: selectedToolId },
        success: function (data) {
            if (data != null) {
                toolFeedback = data;
                feedbackId = data.Id;
            }

            $("#starsFeedback").empty().removeAttr("data-rating");
            $("#starsFeedback").ratingFeedback("create", { value: toolFeedback.Rating });
            $("#textareaUsrFdback").val(toolFeedback.Comment);
        },
        error: function (er) { console.log(er); }
    });
}

function getToolAllFeedbacks() {
    $.ajax({
        async: true,
        dataType: "json",
        url: "../api/feedback/getAllFeedbaksByToolId",
        data: { toolId: selectedToolId },
        success: function (data) {
            usrFeedbacks = data;
            var i = 1;
            var iscolored = true;
            if (usrFeedbacks.length > 0) {
                try {
                    notifyFeedback(previousUserFeedback(usrFeedbacks[0], 0, iscolored), usrFeedbacks.length > 3 ? true : false);
                    $("#feeedbackRating0").ratingFeedback("create", { size: "1.3em", value: usrFeedbacks[0].Rating, isfixed: true });
                }
                catch (e) { }
            }
            setInterval(function () {
                if (usrFeedbacks.length > 0) {
                    try {
                        iscolored = !iscolored;
                        var html = previousUserFeedback(usrFeedbacks[i], i, iscolored);
                        notifyFeedback(html, usrFeedbacks.length > 3 ? true : false);
                        $("#feeedbackRating" + i).ratingFeedback("create", { size: "1.3em", value: usrFeedbacks[i].Rating, isfixed: true });
                        i++;
                        if (usrFeedbacks.length > 3)
                            (i == usrFeedbacks.length) ? i = 0 : i = i;
                    } catch (e) { }
                } else {
                    i = 0;
                }
            }, 3000);
        },
        error: function (er) { console.log(er); }
    });
}

function saveToolFeedbacks() {
    var userFeedback = { id: toolFeedback.Id, toolId: selectedToolId, rating: $("#starsFeedback").attr('data-rating'), comment: $("#textareaUsrFdback").val() };
    $.ajax({
        async: true,
        url: "../api/feedback/mergeFeedback",
        type: 'POST',
        data: JSON.stringify(userFeedback),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            console.log(data); console.log(data.status);
            if (data.status) {
                if (feedbackId == 0) {
                    // usrFeedbacks.push(data.feedback);
                    feedbackId = data.feedback.Id;
                    toolFeedback = data.feedback;
                }
                toolFeedback = data.feedback;
                setFeedbackAlert("WS", "Thanking for your feedback.");
            }
            else {
                setFeedbackAlert("WE", "Feedback saving error.");
            }

        },
        error: function (data) {
            console.log(data);
            result = false;
        }
    });
}

function previousUserFeedback(usrFeedback, arrindex, iscolored) {
    var html = "";
    (iscolored) ? html += '<div style="background: #f7f7f7; padding-top:5px; padding-bottom:5px;"> ' : html += '<div style=" padding-top:5px; padding-bottom:5px;" > ';
    html += '<div style="display: flex;"> <a ><span style="font-size: 15px !important;" class="glyphicon glyphicon-user"></span></a> &nbsp; <span> ' + usrFeedback.Name + '</span> &nbsp; </div>' +
        '<div style="display: flex; padding: 3px;"> &nbsp;<div id="feeedbackRating' + arrindex + '">     </div>  </div> ' +
        '<div>  <div style="margin-left:14px; margin-bottom:6px; font-family: cursive;" > ' + usrFeedback.Comment + ' </div> </div> ' +
        '<div> ';

    return html;
}

function setFeedbackAlert(alrtClass, alrtMssge) {

    if (timeOut != null) clearTimeout(timeOut);

    if (alrtClass == "AS") {
        $("#showNotification").css("color", "#29d827");
    }
    else if (alrtClass == "AI") {
        $("#showNotification").css("color", "#327731");
    }
    else if (alrtClass == "AW") {
        $("#showNotification").css("color", "#b5ca44");
    }
    else if (alrtClass == "AE") {
        $("#showNotification").css("color", "#da7b44")
    }
    $('#showNotification').text(alrtMssge); $("#showNotification").css("visibility", "visible");
    timeOut = setTimeout(function () { $("#showNotification").css("visibility", "hidden"); }, 3000);

}
