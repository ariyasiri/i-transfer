﻿//String Helpers
var singleQuote = String.fromCharCode(39);
var doubleQuote = String.fromCharCode(42);

//Global Variables
 
var baseUrl = "/quizappui";   //to empty url use "" : "/app";....
 
function GetDataValue(inVal) {
    var rtnVal = null;
    if (inVal < 10) {
        rtnVal = "0" + inVal;
    }
    else {
        rtnVal = inVal;
    }
    return rtnVal;
}

function wrapLMSContent(divContent) {
    $("#" + divContent).wrap('<div  class="temp-content"   />');
    $("#" + divContent).each(function (i) { this.style.visibility = "hidden"; });
}

function unwrapLMSContent(divContent) {
    $("#" + divContent).each(function (i) { this.style.visibility = "visible";});
    $("#" + divContent).unwrap();
}

function createContentUUID() {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}
 
var els = {};

    els.common = {};

    els.modules = {};

    els.modules.admin = {}; 
 
    els.modules.quiz = {};

    els.widgets = {};

    els.teacher = {};

    els.student = {};

    els.principal = {};

