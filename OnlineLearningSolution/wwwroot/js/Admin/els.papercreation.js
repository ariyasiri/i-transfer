﻿var els = {};
els.modules = {};
els.modules.admin = {};
els.modules.admin.managePapers = {};

 
(function (context) {

    
    var paperEditor = null;
    var $ddlQualifications = null, $ddlPaperSubject = null, $ddlPaperStage = null, $ddlPaperTerm=null, $ddlPaperRevison=null, $dpPaperYear = null, $numPaperAllctTime = null, $numPaperNoOfQtn = null;
    var $ddlUnit = null, $ddlOrigin = null, $numNoOfAns = null, $numCorctAns = null, $numQtnNo = null, $numAnsAlTime = null;
    var $wndCreateQtn = null, $sliderPaperScale = null, $paperValidator = null, $adminNotification = null, $questionValidator = null, $difficultyValidator = null;
    var qualificationId = 0, selectedPaper = {}; $pagination = $('#paperQuestionNavigation');
    var $paperGrid = null,$ddlQualificationsSh = null, $ddlPaperSubjectSh = null, $ddlPaperStageSh = null, $ddlPaperRevisonSh = null,  $dpPaperYearSh = null;

    var paperlist = [];
    var questionList = [];
 
    var qualificationsList = [], qualificationTermList = [], qualificationStageList = [], qualificationRevisionList = [], qualificationSubjectList = [];
    var courseList = [], unitList = [];
    var typeList = [{ name: "Government", id: "GOVERNMENT" }, { name: "Other", id: "OTHER" }];
    var originList = [{ name: "Government", id: 1 }, { name: "Other", id: 2 }]; 

    context.init = function () {
        $("#paperMain").css("display", "block");
        wrapLMSContent("paperMain");
        $(window).resize(function () {
            context.layoutRefresh();
        }); 
        context.createSearchContent();
       // context.createPaperGrid();

        $("#filePaperFile").on("change", function () {   $paperValidator.validate();   });

        $("#paperButtonNew").on("click", function () {
            $("#paperMain").css("display", "none");
            $("#paperCreateNew").css("display", "block");
            if ($paperValidator == null)
                $paperValidator = $("#formCreatePaper").kendoValidator().data("kendoValidator");
            else
                $paperValidator.hideMessages();

            try {
                $("#filePaperFile").replaceWith($("#filePaperFile").val('').clone(true));
                $numPaperNoOfQtn.value(null); $numPaperAllctTime.value(null); $("#txtPaperDesc").val(''); $("#txtPaperName").val('');
            } catch (e) { };
            context.setPaperDetails(0);          
        });

        $("#buttonSavePaperSh").on("click", function () {
            try {
                wrapLMSContent("paperMain");                
                context.serachPaperList();
            }
            catch (e) { unwrapLMSContent("paperMain");}
        });

        $("#buttonSavePaperResetSh").on("click", function () {
            try {
                 wrapLMSContent("paperMain");
                $ddlQualificationsSh.select(0);
                $dpPaperYearSh.value(null);
                $ddlQualificationsSh.trigger("change");
                context.serachPaperList();
            } catch (e) { unwrapLMSContent("paperMain");   }
        });        
 
        $adminNotification = $("#configurableNotification").kendoNotification({
            position: {
                bottom: 70,
                right: 20
            }
        }).data("kendoNotification");

        $("#buttonSavePaper").on("click", function () {
             if ($paperValidator.validate())
                context.savePaper();             
        });

        $("#buttonSavePaperCancel").on("click", function () {
            $("#paperMain").css("display", "block"); 
            $("#paperCreateNew").css("display", "none"); context.layoutRefresh();
        });

        $("#paperCancelQuestion").on("click", function () {
            $("#paperMain").css("display", "block");
            $("#paperQuestionContent").css("display", "none"); context.layoutRefresh();
        });

        $("#paperAddQuestion").on("click", function () {   
            context.createdQuestionListContent();
            context.paperAddQuestion(false); 
        });

        $("#btnCancelAddQuestions").on("click", function () {
            if (questionList.length > 0) {
                $("#paperContentCreation").css("display", "none");
                $("#paperQuestionContent").css("display", "block");
            }
            else {
                $("#paperMain").css("display", "block"); 
                $("#paperContentCreation").css("display", "none"); context.layoutRefresh();
            }
        });

        $("#buttonRfrshPaper").on("click", function () {
            dataPdf();
            readPdf();

            pageNum = 1;
            try {
                queueRenderPage(pageNum);
            } catch (e) { }

            setTimeout(function () { setContentData(); }, 1000);
        });

        $("#buttonSaveQuestionCancel").on("click", function () {
           // $wndCreateQtn.close();
            $("#paperContentCreation").css("display", "block");
            $("#wndAddEditQuestion").css("display", "none");
        });

        $("#buttonSaveQuestion").on("click", function () {
          if ($questionValidator.validate() && $difficultyValidator.validate())  
              context.saveQuestion();
            
        });

        $("#buttonDownloadQImage").click(function () {
            download_image();
        });

        $("#btnAddQuestion").click(function () {
            context.AddQuestionContent();
            setCroppedImage();
 
            $("#paperContentCreation").css("display", "none");
            $("#wndAddEditQuestion").css("display", "block");

            $("#imgCroppedContainer").height((($(window).height() - ($('#wndAddEditQuestion .row').height() * 5) - 280) < 160) ? 160 : ($(window).height() - ($('#wndAddEditQuestion .row').height() * 5) - 280));
            $("#imgPaperCroppedQuestion").height((($(window).height() - ($('#wndAddEditQuestion .row').height() * 5) - 280) < 160) ? 160 : ($(window).height() - ($('#wndAddEditQuestion .row').height() * 5) - 280));
 
        });
    };

    context.layoutRefresh = function () {
        try {
            $("#gridContainer").height(($(window).height() - $("#formCreatePaperSh").height() - 240) < 120 ? 120 : ($(window).height() - $("#formCreatePaperSh").height() - 240));
            $paperGrid.refresh();
        }
        catch (e) { }

        try {
            $("#imgCroppedContainer").height((($(window).height() - ($('#wndAddEditQuestion .row').height() * 5) - 280) < 160) ? 160 : ($(window).height() - ($('#wndAddEditQuestion .row').height() * 5) - 280));
            $("#imgPaperCroppedQuestion").height((($(window).height() - ($('#wndAddEditQuestion .row').height() * 5) - 280) < 160) ? 160 : ($(window).height() - ($('#wndAddEditQuestion .row').height() * 5) - 280));
        } catch (e) { }
    };

    context.serachPaperList = function () {
        var strParam = "qualificationId=" + $ddlQualificationsSh.value();
        if ($dpPaperYearSh.value() != null)
            strParam += " &year=" + $dpPaperYearSh._oldText;
        try {
            if ($ddlPaperSubjectSh.value() > 0)
                strParam += " &qualificationSubjectId=" + $ddlPaperSubjectSh.value();
        } catch (e) { };
        try {
            if ($ddlPaperStageSh.value() > 0)
                strParam += " &qualificationStageTermId=" + $ddlPaperStageSh.value();
        } catch (e) { };
        try {
            if ($ddlPaperRevisonSh.value() > 0)
                strParam += " &qualificationRevisionId=" + $ddlPaperRevisonSh.value();
        } catch (e) { };
        strParam += "&pageSize=40 &pageIndex=1 &totalNoOfRecs=500";

        $.ajax({
            type: "Get",
            url: baseUrl+ "/api/admin/GetPapersByParam",
            dataType: 'JSON', async: true,
            data: { paramArgs: strParam},
            success: function (data) {
                unwrapLMSContent("paperMain");
                paperlist = data.result;
                context.createPaperGrid();
            },
            error: function (e) {
                unwrapLMSContent("paperMain");
            }
        });

    };

    context.createSearchContent = function () {
        context.getQualifications();
        if ($ddlQualificationsSh == null)
            $ddlQualificationsSh = $("#ddlPaperQalificationSh").kendoDropDownList({
                dataTextField: "name",
                dataValueField: "id",
                dataSource: qualificationsList,
                index: 0,
                change: context.changeQualification
            }).data("kendoDropDownList");
 
            $ddlQualificationsSh.select(0);
            context.setQualifsationChange();     

        if ($dpPaperYearSh == null)
            $dpPaperYearSh = $("#datePaperYearSh").kendoDatePicker({
                start: "decade",
                depth: "decade",
                format: "yyyy"
            }).data("kendoDatePicker");
        try {
            context.serachPaperList();
        }
        catch (e) { unwrapLMSContent("paperMain");}
    };

    context.getPaperQuestions = function () {
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetPaperQuestionById",
            data: { paperId: selectedPaper.paperId },
            dataType: 'JSON',
            async: true,
            success: function (data) { 
                questionList = data;
                context.createPagination(1, selectedPaper.noOfQuestions);
                if (questionList != undefined)
                    context.findAndSetSelectedQuestion(1); 
            }
        });   
    };

    context.saveQuestion = function () {
         var msg = "";
        $("#buttonSaveQuestion").attr("disabled", true);
        try {

            wrapLMSContent("formAddEditQuestion");

            var att = new FormData(); 
            att.append("PaperId", selectedPaper.paperId);
            att.append("QuestionOrder", $numQtnNo.value());
            att.append("DifficultyLevel", $('input[name="radioAnsDiffLevel"]:checked').val());
            att.append("Origin", $ddlOrigin.value());
            att.append("AllocatedTime", $numAnsAlTime.value());
            att.append("NoOfAnswers", $numNoOfAns.value());
            att.append("CorrectAnswer", $numCorctAns.value());
            att.append("Description", $("#txtAnsDesc").val());
            att.append("QuestionImg", $("#imgPaperCroppedQuestion").prop("src"));
            att.append("Units", $ddlUnit.value().toString());
 

            var ajaxRequest = $.ajax({
                type: "POST",
                dataType: 'json',
                url: baseUrl + "/api/admin/SaveQuestionData",
                contentType: false,
                processData: false,
                aync: true,
                data: att,
                success: function (data) { 
                    $("#buttonSaveQuestion").attr("disabled", false); 
 
                    unwrapLMSContent("formAddEditQuestion");
                    $("#paperContentCreation").css("display", "block");
                    $("#wndAddEditQuestion").css("display", "none");
                     
                    questionList.push(data); 
                    if (questionList != null) {
                        context.createdQuestionListContent();
                        context.createPagination(data.questionOrder, questionList[questionList.length - 1].questionOrder);
                    }
                },
                error: function (data) {
                    var msg = "Invalid file"; $("#buttonSaveQuestion").attr("disabled", false);
                    unwrapLMSContent("formAddEditQuestion");
                }
            });
        }
        catch (e) { unwrapLMSContent("formAddEditQuestion"); $("#buttonSaveQuestion").attr("disabled", false); }
    };

    context.createdQuestionListContent = function () {
        if (questionList != null) {
            questionList.sort(function (a, b) {
                return parseFloat(a.questionOrder) - parseFloat(b.questionOrder);
            });
            var htmlCnt = "";
            for (var i = 0; i < questionList.length; i++) {
                htmlCnt += '<span>' + questionList[i].questionOrder + '</span>';
            }
            $(".els-added-question").empty().append(htmlCnt);
        }
    };

    context.createPagination = function (questionno, noOfQuestions) {
        try { $('#paperQuestionNavigation').pagination('destroy'); } catch (e) {  }
        $('#paperQuestionNavigation').pagination({
            pages: 1,
            cssStyle: 'light-theme',
            prevText: "<",
            nextText: ">",
            onPageClick: function (e) {
                context.findAndSetSelectedQuestion(e);
            }
        });
         
        $('#paperQuestionNavigation').pagination('updateItems', noOfQuestions);

        $('#paperQuestionNavigation').pagination('selectPage', questionno);
    };
       
    context.paperAddQuestion = function (isaddPaper) {

        if (!isaddPaper) {
            setTimeout(function () {
                $("#paperContentCreation").css("display", "block");
                $("#paperQuestionContent").css("display", "none");}, 2000);
        }
         
        

        if ($sliderPaperScale == null) {
            $sliderPaperScale = $("#sliderPaperScale").kendoSlider({
                change: context.sliderScaleOnChange,
                min: 1,
                max: 10,
                smallStep: 1,
                largeStep: 1,
                value: 5
            }).data("kendoSlider");
        } 
        //need to romove
       // selectedPaper.paperLocation = "http://173.82.114.87:3000/questions/2019/8/7/190418%20ts_637007853650606441.pdf";
        urlPdfData = selectedPaper.paperLocation;
        dataPdf();
        readPdf();

        pageNum = 1;
        try {
            queueRenderPage(pageNum);
        } catch (e) { }

        setTimeout(function () { setContentData(); }, 2000);
    };

    context.savePaper = function () { 
        try {
            var msg = "";
            $("#buttonSavePaper").attr("disabled", true); wrapLMSContent("paperCreateNew");
            var fileInput = document.getElementById("filePaperFile");
            // var fileInput = $("#filePaperFile").data("kendoUpload").getFiles();
            var files = $("#filePaperFile").get(0).files;
            if (files.length > 0) {
               
                    var file = files[0];
                    var att = new FormData();
                    att.append("FileName", file.name);
                    att.append("Name", $("#txtPaperName").val());
                    att.append("QualificationSubjectId", $ddlPaperSubject.value());
                    att.append("QualificationStageTermId", $ddlPaperTerm.value());
                    att.append("QualificationRevisionId", $ddlPaperRevison.value());
                    att.append("AllocatedTimeInMinutes", $numPaperAllctTime.value());
                    att.append("NoOfQuestions", $numPaperNoOfQtn.value());
                    att.append("Year", $("#datePaperYear").data("kendoDatePicker")._oldText);
                    att.append("Description", $("#txtPaperDesc").val());
                    att.append("UploadingPaper", file);

                    var ajaxRequest = $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: baseUrl + "/api/admin/SavePaperData",
                        contentType: false,
                        processData: false,
                        aync: true,
                        data: att,
                        success: function (data) {
                            $("#buttonSavePaper").attr("disabled", false); 
                            selectedPaper = data;
                            if (selectedPaper != undefined) {
                                var subjectid = $ddlPaperSubject.value();
                                selectedPaper.subjectId = subjectid;
                                selectedPaper.stage = $ddlPaperStage.text();
                                selectedPaper.term = $ddlPaperTerm.text();
                                $paperGrid.dataSource.data().splice(0, 0, selectedPaper);
                                $("#paperCreateNew").css("display", "none");
                                questionList = [];
                                context.paperAddQuestion(true);
                                console.log('YYY');
                                setTimeout(function () {                                                                     
                                    unwrapLMSContent("paperCreateNew"); console.log('ZZZZ');
                                    $("#paperContentCreation").css("display", "block");
                                    $("#paperQuestionContent").css("display", "none");
                                    
                                }, 2000);
                            }
                        },
                        error: function (data) {
                            alert('2');
                            var msg = "Invalid file";
                            $("#buttonSavePaper").attr("disabled", false); unwrapLMSContent("paperCreateNew");
                        }
                    });
               
            }
            else {
                msg = "Please select file to upload";
                $("#buttonSavePaper").attr("disabled", false); unwrapLMSContent("paperCreateNew");
            }

        }
        catch (e) { $("#buttonSavePaper").attr("disabled", false); unwrapLMSContent("paperCreateNew"); alert('0111');}
    };

    context.getQualifications = function () {
        $.ajax({
            type: "Get",
            url: baseUrl+ "/api/admin/GetAllQualifications",
            dataType: 'JSON', async: false,
            success: function (data) {
                qualificationsList = data;
                qualificationId = data[0].id;
            }
        });
    };

    context.AddQuestionContent = function () { 
        if ($questionValidator == null)
            $questionValidator = $("#formAddEditQuestion").kendoValidator().data("kendoValidator");
        else
            $questionValidator.hideMessages();

        if ($difficultyValidator == null)
            $difficultyValidator = $("#formDifficultyLevel").kendoValidator({
                rules: {
                    radio: function (input) {
                        if (input.filter("[type=radio]") && input.attr("required")) {
                            return $("#formDifficultyLevel").find("[type=radio][name=radioAnsDiffLevel]").is(":checked");
                        }
                        return true;
                    }
                },
                messages: {
                    radio: "Select Difficulity Level"
                }
            }).data("kendoValidator");
        else
            $difficultyValidator.hideMessages();

        var selctedSubjectedId = selectedPaper.subjectId;         
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetUnitsBySubject",
            data: { subjectId: selctedSubjectedId},
            dataType: 'JSON',
            async: true,
            success: function (data) {
                if ($ddlUnit == null)
                    $ddlUnit = $("#multiselectAnsUnit").kendoMultiSelect({
                        placeholder: "Select unit(s)...",
                        dataTextField: "name",
                        dataValueField: "unitId",
                        autoBind: false,
                        dataSource: data
                    }).data("kendoMultiSelect");
            }
        });       

        if ($ddlOrigin == null)
            $ddlOrigin = $("#ddlAnsOrigin").kendoDropDownList({
                dataTextField: "name",
                dataValueField: "id",
                dataSource: originList,
                index: 0
            }).data("kendoDropDownList");

        if ($numNoOfAns == null)
            $numNoOfAns = $("#numAnswers").kendoNumericTextBox({ min: 1, format: "N0", change: function (e) { if (this.value() != null) { $numCorctAns.max(this.value()); try { ($numCorctAns.value() <= this.value()) ? $numCorctAns.value() : $numCorctAns.value(null); } catch (e) { } }} }).data("kendoNumericTextBox");
        if ($numAnsAlTime == null)
            $numAnsAlTime = $("#numAnsAlctTime").kendoNumericTextBox({ min: 1, format: "N0" }).data("kendoNumericTextBox");
        if ($numCorctAns == null)
            $numCorctAns = $("#numAnsCorrectAns").kendoNumericTextBox({ min: 1, format: "N0" }).data("kendoNumericTextBox");
        if ($numQtnNo == null)
            $numQtnNo = $("#numAnsQuestionNo").kendoNumericTextBox({
                min: 1, format: "N0", change: function (e) { 
                    if (this.value != null) {
                        for (var i = 0; i < questionList.length; i++) {   if (questionList[i].questionOrder == this.value()) { $numQtnNo.value(null); break; } }
                    }
                }
            }).data("kendoNumericTextBox");

        try {
            $numQtnNo.value(null); $numAnsAlTime.value(null); $numCorctAns.value(null); $ddlUnit.value(null); $numNoOfAns.value(null);
            $("#txtAnsDesc").val(''); $('input[name="radioAnsDiffLevel"]:checked').removeAttr("checked");
            $numNoOfAns.max(selectedPaper.noOfQuestions);
        } catch (e) { }
    };

    context.getQualificationStages = function () { 
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetQualificationStages",
            data: { qualificationId: qualificationId },
            dataType: 'JSON',
            async: true,
            success: function (data) { 
                qualificationStageList = data;
                if ($ddlPaperStage == null)
                    $ddlPaperStage = $("#ddlPaperStage").kendoDropDownList({
                        dataTextField: "stage",
                        dataValueField: "id",
                        dataSource: [],
                        //index: 0
                    }).data("kendoDropDownList");

                if ($ddlPaperStageSh == null)
                    $ddlPaperStageSh = $("#ddlPaperStageSh").kendoDropDownList({
                        dataTextField: "stage",
                        dataValueField: "id",
                        dataSource: [],
                        //index: 0
                    }).data("kendoDropDownList");

                $ddlPaperStage.dataSource.data(qualificationStageList);
                $ddlPaperStage.select(0);

                var tempList = qualificationStageList;
                tempList.unshift({ stage: "All Stages", id: 0 });
                $ddlPaperStageSh.dataSource.data(tempList);
                $ddlPaperStageSh.select(0);
            }

        });
    };

    context.getQualificationTerms = function () { 
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetQualificationStageTerms",
            dataType: 'JSON',
            data: { qualificationId: qualificationId },
            async: true,
            success: function (data) { 
                qualificationTermList = data;

                if ($ddlPaperTerm == null)
                    $ddlPaperTerm = $("#ddlPaperTerm").kendoDropDownList({
                        dataTextField: "term",
                        dataValueField: "id",
                        dataSource: []
                        // index: 0
                    }).data("kendoDropDownList");

                $ddlPaperTerm.dataSource.data(qualificationTermList);
                $ddlPaperTerm.select(0);
            }
        });
    };

    context.GetAllQualificationSubjects = function () { 
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetQualificationSubjects",
            dataType: 'JSON',
            data: { qualificationId: qualificationId },
            async: true,
            success: function (data) { 
                qualificationSubjectList = data;
                if ($ddlPaperSubject == null)
                    $ddlPaperSubject = $("#ddlPaperQlftnSuj").kendoDropDownList({
                        dataTextField: "name",
                        dataValueField: "qualificationSubjectId",
                        dataSource: []
                        // index: 0
                    }).data("kendoDropDownList");

                if ($ddlPaperSubjectSh == null)
                    $ddlPaperSubjectSh = $("#ddlPaperQlftnSujSh").kendoDropDownList({
                        dataTextField: "name",
                        dataValueField: "qualificationSubjectId",
                        dataSource: []
                        // index: 0
                    }).data("kendoDropDownList");

                $ddlPaperSubject.dataSource.data(qualificationSubjectList);
                $ddlPaperSubject.select(0); 
                var tempList = qualificationSubjectList;
                tempList.unshift({ name: "All Subjects", qualificationSubjectId: 0 });
                $ddlPaperSubjectSh.dataSource.data(tempList);
                $ddlPaperSubjectSh.select(0);
            }
        });
    };

    context.getQualificationRevision = function () { 
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetQualificationRevisions",
            dataType: 'JSON',
            async: true,
            data: { qualificationId: qualificationId },
            success: function (data) { 
                qualificationRevisionList = data;
                if ($ddlPaperRevison == null)
                    $ddlPaperRevison = $("#ddlPaperRevision").kendoDropDownList({
                        dataTextField: "revision",
                        dataValueField: "qualificationRevisionId",
                        dataSource: []
                        //index: 0
                    }).data("kendoDropDownList");
                if ($ddlPaperRevisonSh == null)
                    $ddlPaperRevisonSh = $("#ddlPaperRevisionSh").kendoDropDownList({
                        dataTextField: "revision",
                        dataValueField: "qualificationRevisionId",
                        dataSource: []
                        //index: 0
                    }).data("kendoDropDownList");

                $ddlPaperRevison.dataSource.data(qualificationRevisionList);
                $ddlPaperRevison.select(0);

                var tempList = qualificationRevisionList;
                tempList.unshift({ revision: "All Revisions", qualificationRevisionId: 0 });
                $ddlPaperRevisonSh.dataSource.data(tempList);
                $ddlPaperRevisonSh.select(0);
            }
        });
    };

    context.sliderScaleOnChange = function (e) { 
        scale = e.value;
    };

    context.createPaperGrid = function () {
        $('#paperGrid').empty(); 
        paperlist.sort(function (a, b) {      //order by descending
            return parseFloat(b.paperId) - parseFloat(a.paperId);
        });
 
        $paperGrid = $('#paperGrid').kendoGrid({
            dataSource:
            {
                data: paperlist,
                pageSize: 10
            },

            columns: [
                {
                    title: "Name",
                    field: "name"
                },
                {
                    title: "Year",
                    field: "year",
                    width: 60
                },
                {
                    title: "Stage",
                    field: "stage"
                },
                {
                    title: "Term",
                    field: "term"
                },
                {
                    template: '<button type="button" class="common-button" data-paperid="#: paperId#" data-noofq="#: noOfQuestions#" data-plocation="#: paperLocation#" data-subjectid="#: subjectId#" onclick="els.modules.admin.managePapers.editPaper(this,1)">Questions</button> ' +
                        '<button type="button" class="common-button" data-paperid="#: paperId#" onclick="els.modules.admin.managePapers.editPaper(this)">Edit</button> ' +
                        '<button type="button" class="common-button" data-paperid="#: paperId#"  onclick="els.modules.admin.managePapers.deletePaper(this)">Delete</button> ',
                    width: 200,
                    attributes: { style: "text-align: center;" }
                }
            ],
            pageable: {
                pageSizes: true,
                buttonCount: 10
            }
        }).data('kendoGrid');

        context.layoutRefresh();
        
    };

    context.setQualifsationChange = function () { 
        context.getQualificationStages();
        context.getQualificationTerms();
        context.GetAllQualificationSubjects();
        context.getQualificationRevision(); 
    };

    context.changeQualification = function (e) {
        qualificationId = this.dataItem().id;
        if (qualificationId > 0) {
            context.setQualifsationChange();
        }
    };

    context.setPaperDetails = function (paperId) { 
        context.getQualifications();
        if ($ddlQualifications == null)
            $ddlQualifications = $("#ddlPaperQalification").kendoDropDownList({
                dataTextField: "name",
                dataValueField: "id",
                dataSource: qualificationsList,
                index: 0,
                change: context.changeQualification
            }).data("kendoDropDownList"); 
        if (paperId == 0) {
            $ddlQualifications.select(0);
            context.setQualifsationChange();
        }

        if ($dpPaperYear == null)
            $dpPaperYear = $("#datePaperYear").kendoDatePicker({
                start: "decade",
                depth: "decade",
                format: "yyyy"
            }).data("kendoDatePicker");

        if ($numPaperAllctTime == null)
            $numPaperAllctTime = $("#numPaperAllctTime").kendoNumericTextBox({ min: 1, format: "N0" }).data("kendoNumericTextBox");

        if ($numPaperNoOfQtn == null)
            $numPaperNoOfQtn = $("#numPaperNoOfQstn").kendoNumericTextBox({ min: 1, format: "N0" }).data("kendoNumericTextBox");

    };

  
    context.deletePaper = function (button) { 
    };

    context.editPaper = function (button, questionno) {
 
        selectedPaper.paperLocation = $(button).data('plocation');
        selectedPaper.paperId = $(button).data('paperid'); 
        selectedPaper.noOfQuestions = $(button).data('noofq');
        selectedPaper.subjectId = $(button).data('subjectid');

        $("#paperMain").css("display", "none");
        $("#paperQuestionContent").css("display", "block");

        questionList = [];
        context.getPaperQuestions();  
    };

    context.getCorretAnswer = function (selectedQtn) {
        $.ajax({
            type: "Get",
            url: baseUrl + "/api/admin/GetQuestionAnswer",
            dataType: 'JSON',
            data: { questionId: selectedQtn.question.questionId },
            async: true,
            success: function (data) { 
                if (data != null) {
                    for (var i = 0; i < data.length; i++) { 
                        if (data[i].isCorrectAnswer) { 
                            selectedQtn.question.correctAnswer = data[i].answerOrder;
                            context.setQuetionContent(selectedQtn); 
                            break;
                        }
                    }
                }
            }
        });
    };

    context.findAndSetSelectedQuestion = function (questionno) { 

        var selectedQtn = undefined;
        for (var i = 0; i < questionList.length; i++) {
            if (questionList[i].questionOrder == questionno) {
                selectedQtn = questionList[i];
                break;
            }
        }
          
        $("#paperSelQtnContent").empty();
        if (selectedQtn != undefined) {
             
            if (selectedQtn.question.correctAnswer == undefined)
                selectedQtn.correctAnswer = context.getCorretAnswer(selectedQtn);
            else
                context.setQuetionContent(selectedQtn);
            
        }
        else {
            $("#paperEditQuestion").css("display", "none");
            $("#paperSelQtnContent").append('<div style="text-align: center; height: 340px; object - fit: contain;"><strong>Question has not been added for selected question number.</strong></div>');
        }

    };

    context.setQuetionContent = function (selectedQtn) {
        $("#paperSelQtnContent").empty();
        $("#paperEditQuestion").css("display", "inline-block");

        $("#paperSelQtnContent").append('<div style="text-align:center; height:280px; "><img class="imageQContent" style="height:275px;object-fit: scale-down !important;" src="' + selectedQtn.question.questionImageLocation + '" /> </div>');
        var ansStrList = '<br/> <div class="els-radio-answer" >';
        for (i = 1; i <= selectedQtn.question.noOfAnswers; i++) {
            if (selectedQtn.question.correctAnswer == i)
                ansStrList += '<input type="radio" name="paperQtnAwr"   class="k-radio" checked>       <label class="k-radio-label" for="engine2"> ' + i + '</label>';
            else
                ansStrList += '<input type="radio" name="paperQtnAwr"   class="k-radio">       <label class="k-radio-label" for="engine2"> ' + i + '</label>';

        }
        ansStrList += '</div> <br/>';
        $("#paperSelQtnContent").append(ansStrList);
        $("#imageQContent").css("object-fit", "scale-down !important");
    };

})(els.modules.admin.managePapers);


