﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLearningSolution.Models
{
    public class UserLogin
    {
        public UserLogin()
        {
            Institutes = new List<Institute>();
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FamilyName { get; set; }

        public string OtherNames { get; set; }

        public System.Nullable<DateTime> Birthday { get; set; }

        public string Code { get; set; }

        public string Gender { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public string SelectedInstitute { get; set; }

        public int SelectedInstituteID { get; set; }

        public  List<Institute> Institutes{ get; set; }

        public int QualificationID { get; set; }
    }
}
