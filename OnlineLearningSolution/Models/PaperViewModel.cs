﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLearningSolution.Models
{ 

    public class Qulification
    {
        public int id { get; set; }

        public string name { get; set; }

    }

    public class QulificationStage
    {
        public int Id { get; set; }

        public string Stage { get; set; }

    }

    public class QulificationTerm
    {
        public int Id { get; set; }

        public string Term { get; set; }
    }

    public class SubjectUnit
    {
        public int SubjectId { get; set; }

        public Unit Unit { get; set; }
    }

    public class Unit
    {
        public int UnitId { get; set; }

        public string Name { get; set; }
    }
     
    public class QulificationSubject
    {
        public int qualificationSubjectId { get; set; }

        public Subject Subject { get; set; }

        public string Name
        {
            get
            {
                return Subject.Name;
            }
        }
    }

    public class Subject
    {
        public int SubjectId { get; set; }

        public string Name { get; set; }
    }

    public class QulificationRevision
    {
        public int qualificationRevisionId { get; set; }

        public string revision { get; set; }
    }

    public class PaperSearchResult
    {
        public List<PaperSearch> Result { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public int TotalNoOfRecs { get; set; }
    }

    public class PaperSearch
    {
        public int PaperId { get; set; }

        public string PaperLocation { get; set; }

        public string Name { get; set; }

        public int Year { get; set; }

        public int AllocatedTimeInMinutes { get; set; }

        public int NoOfQuestions { get; set; }

        public string Description { get; set; }

        public int SubjectId { get; set; }

        public int QualificationId { get; set; }

        public string Revision { get; set; }

        public string Stage { get; set; }

        public string Term { get; set; }
    }

    public class Paper
    {
        public int PaperId { get; set; }

        public string Name { get; set; }

        public string PaperLocation { get; set; }

        public int QualificationSubjectId { get; set; }

        public int QualificationStageTermId { get; set; }

        public int QualificationRevisionId { get; set; }

        public int AllocatedTimeInMinutes { get; set; }

        public int NoOfQuestions { get; set; }

        public int Year { get; set; }

        public string Description { get; set; }

        public string FileName { get; set; }

        public byte[] UploadingPaper { get; set; }
    }
    
    public class PaperQuestion
    {
        public int PaperId { get; set; }

        public int QuestionOrder { get; set; }

        public string Description { get; set; }

        public Question Question { get; set; }
    }

    public class Question
    {
        public int QuestionId { get; set; }

        public string QuestionImageLocation { get; set; }

        public string QuestionText { get; set; }

        public string DifficultyLevel { get; set; }

        public string Origin { get; set; }

        public int AllocatedTime { get; set; }

        public int NoOfAnswers { get; set; }

        public int NoOfCorrectAnswers { get; set; }

        public int CorrectAnswer { get; set; }

        public string Description { get; set; }

        public byte[] QuestionImg { get; set; }

        public string Units { get; set; }
    }

    public class PaperQuestionModel
    {
        public int PaperId { get; set; }

        public int QuestionOrder { get; set; }

        public string Description { get; set; }

        public QuestionModel Question { get; set; }
    }

    public class QuestionModel
    {
        public int QuestionId { get; set; }

        public string QuestionText { get; set; }

        public string DifficultyLevel { get; set; }

        public string Origin { get; set; }

        public int BestTimeInMinutes { get; set; }

        public int AverageTime { get; set; }

        public int AllocatedTime { get; set; }

        public int NoOfAnswers { get; set; }

        public int NoOfCorrectAnswers { get; set; }

        public string Description { get; set; }

        public string QuestionImageLocation { get; set; }

    }
   
    public class QuestionAnswer
    {
        public int QuestionId { get; set; }

        public int AnswerId { get; set; }

        public string AnswerText { get; set; }

        public string Description { get; set; }

        public int AnswerOrder { get; set; }

        public bool IsCorrectAnswer { get; set; }

    }
}
