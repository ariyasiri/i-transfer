﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLearningSolution.Models
{
    public class Student 
    {
       public string GuardianName  {get; set;}
        public string GuardianEmail { get; set;}
        public string GuardianPhoneNo { get; set;}
        public string GuardianRelationShip { get; set;}
        public string Code { get; set;}
        public string FirstName { get; set;}
        public string LastName { get; set;}
        public string FamilyName { get; set;}
        public string OtherNames { get; set;}
        public string Birthday { get; set;}
        public string UserGender { get; set;}
        public string Description { get; set;}
        public string CountryId { get; set;}
        public string NickName { get; set;}
        public string UserName { get; set;}
        public string Password { get; set;}
        public string PasswordHint { get; set;}
    }
}
