﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLearningSolution.Models
{
    public class CreateQuizViewModel
    {
        public int QualifiacatioId { get; set; }

        public int Subject { get; set; }

        public List<int> UnitList { get; set; }

        public int StageId { get; set; }

        public int TermId { get; set; }

        public int YearForm { get; set; }

        public int YearTo { get; set; }

        public int Type { get; set; }

        public int Time { get; set; }

        public int NoOfQuestions { get; set; }

        public int QuizId { get; set; }
    }
}
