﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLearningSolution.Models
{
    public interface ITokenProvider
    {
        string IsTokenNullOrExpired();

        string GetTokenAsync();
    }
}
