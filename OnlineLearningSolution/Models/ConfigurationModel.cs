﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLearningSolution.Models
{
    public class ElearningService
    {
        public string ApiVersion { get; set; }

        public string BaseUri { get; set; }

        public string PathBase { get; set; }
    }
}
