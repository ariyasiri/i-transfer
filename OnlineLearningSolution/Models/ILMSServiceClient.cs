﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLearningSolution.Models
{
    public interface ILMSServiceClient
    {
        Task<T> GetAsync<T>(string uri, string clientId);

        Task<T> PostAsJsonAsync<T>(object data, string uri, string clientId);

        Task PostAsJsonAsync(object data, string uri, string clientId);

        Task PutAsJsonAsync(object data, string uri, string clientId);

        Task<T> PutAsJsonAsync<T>(object data, string uri, string clientId);
    }
}
